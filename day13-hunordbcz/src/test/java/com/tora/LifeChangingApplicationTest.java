package com.tora;

import com.tora.exceptions.InvalidInputException;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.io.File;
import java.util.Collections;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class LifeChangingApplicationTest {

    @Rule
    public final ExpectedException exception = ExpectedException.none();
    private LifeChangingApplication app;

    @Before
    public void setUp() throws Exception {
        File file = new File("test");
        if (!file.exists()) {
            FileWriter fileWriter = new FileWriter("test");
            fileWriter.write("Cristina|Radu|Kiraly|1900919162261|Cristina.Radu.Kiraly@msdn.com#");
            fileWriter.close();
        }
        PersonRepository personRepository = new PersonRepository();
        app = new LifeChangingApplication("test", personRepository);
    }

    @Test
    public void parseInput() {
        app.parseInput();
        assertThat(app.getPeople().get(0), is(new Person("Cristina", "Radu", "Kiraly", 1900919162261L, "Cristina.Radu.Kiraly@msdn.com")));
    }

    @Test(expected = Test.None.class)
    public void checkSuccess() throws InvalidInputException {
        app.check("Hunor", 1);
    }


    @Test
    public void checkError() throws InvalidInputException {
        exception.expect(InvalidInputException.class);
        app.check("123", 1);
    }

    @Test
    public void getPeople() {
        app.parseInput();
        assertThat(app.getPeople(), is(Collections.singletonList(new Person("Cristina", "Radu", "Kiraly", 1900919162261L, "Cristina.Radu.Kiraly@msdn.com"))));
    }

    @After
    public void tearDown() throws Exception {
        app.close();
        File file = new File("test");
        file.delete();

        File fileErrors = new File("test-errors.txt");
        fileErrors.delete();
    }
}