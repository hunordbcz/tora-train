package com.tora;

import java.util.ArrayList;
import java.util.List;

public class PersonRepository {
    private final List<Person> list;

    public PersonRepository() {
        this.list = new ArrayList<>();
    }

    public synchronized Person add(Person person) {
        list.add(person);
        return person;
    }

    public List<Person> getPeople() {
        return list;
    }
}
