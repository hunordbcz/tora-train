package com.tora;

import com.tora.exceptions.InvalidInputException;

import java.util.List;

public class LifeChangingApplication {
    private final FileReader fileReader;
    private final FileWriter fileWriter;
    private final PersonRepository repository;

    public LifeChangingApplication(String input, PersonRepository personRepository) {
        fileReader = new FileReader(input);
        fileWriter = new FileWriter(input + "-errors.txt");
        repository = personRepository;
    }

    public void parseInput() {
        while (fileReader.hasNext()) {
            String data = fileReader.next();
            String[] tokens = data.split("\\|");

            try {
                for (int i = 0; i < tokens.length; i++) {
                    check(tokens[i], i);
                }
            } catch (InvalidInputException exception) {
                fileWriter.writeln(data + "\n--- " + exception.getMessage());
            }

            repository.add(new Person(tokens));
        }

        close();
    }

    public void check(String expression, int nth) throws InvalidInputException {
        InputType type = null;

        switch (nth) {
            case 0:
            case 1:
            case 2:
                type = InputType.NAME;
                break;
            case 3:
                type = InputType.ID;
                break;
            case 4:
                type = InputType.EMAIL;
                break;
            default:
                break;
        }

        assert type != null;
        type.validate(expression);
    }

    public List<Person> getPeople() {
        return repository.getPeople();
    }

    public void close() {
        fileWriter.close();
        fileReader.close();
    }
}
