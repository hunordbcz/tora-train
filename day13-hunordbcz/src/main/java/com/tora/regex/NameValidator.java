package com.tora.regex;

public class NameValidator extends AbstractValidator {

    @Override
    protected String getRule() {
        return "[a-zA-Z]+|-";
    }
}
