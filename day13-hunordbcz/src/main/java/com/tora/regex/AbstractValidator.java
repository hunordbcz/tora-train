package com.tora.regex;

import java.util.function.Predicate;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public abstract class AbstractValidator implements Predicate<String> {
    @Override
    public boolean test(String s) {
        Pattern p = Pattern.compile(getRule());
        Matcher m = p.matcher(s);
        return m.matches();
    }

    protected abstract String getRule();
}
