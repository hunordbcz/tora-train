package com.tora.regex;

public class EmailValidator extends AbstractValidator {

    @Override
    protected String getRule() {
        return "^[\\w!#$%&'*+/=?`{|}~^-]+(?:\\.[\\w!#$%&'*+/=?`{|}~^-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$";
    }
}
