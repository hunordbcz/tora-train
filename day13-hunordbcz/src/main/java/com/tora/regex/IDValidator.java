package com.tora.regex;

public class IDValidator extends AbstractValidator {

    @Override
    protected String getRule() {
        return "^\\d{13}$";
    }
}
