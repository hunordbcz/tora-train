package com.tora;

import java.util.Objects;

public class Person {
    private final String familyName;
    private final String surName;
    private final String middleName;
    private final Long id;
    private final String email;

    public Person(String familyName, String surName, String middleName, Long id, String email) {
        this.familyName = familyName;
        this.surName = surName;
        this.middleName = middleName;
        this.id = id;
        this.email = email;
    }

    public Person(String[] data) {
        this.familyName = data[0];
        this.surName = data[1];
        this.middleName = data[2];
        this.id = Long.parseLong(data[3]);
        this.email = data[4];
    }

    public String getFamilyName() {
        return familyName;
    }

    public String getSurName() {
        return surName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public Long getId() {
        return id;
    }

    public String getEmail() {
        return email;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Person person = (Person) o;
        return Objects.equals(familyName, person.familyName) &&
                Objects.equals(surName, person.surName) &&
                Objects.equals(middleName, person.middleName) &&
                Objects.equals(id, person.id) &&
                Objects.equals(email, person.email);
    }

    @Override
    public int hashCode() {
        return Objects.hash(familyName, surName, middleName, id, email);
    }
}
