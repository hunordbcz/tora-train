package com.tora;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

public class ThreadManager {
    private final List<String> inputs;
    private final PersonRepository personRepository;
    private int nrThreads;

    public ThreadManager(int nrThreads, List<String> inputs) {
        this.nrThreads = nrThreads;
        this.inputs = inputs;
        this.personRepository = new PersonRepository();
    }

    public void run() {
        List<List<String>> threadInputs = new ArrayList<>();
        if (inputs.size() < nrThreads) {
            nrThreads = inputs.size();
        }

        IntStream.range(0, nrThreads).forEach(i -> {
            threadInputs.add(new ArrayList<>());
        });

        for (int nr = 0; nr < inputs.size(); nr++) {
            threadInputs.get(nr % inputs.size() % nrThreads).add(inputs.get(nr));
        }

        long start = System.currentTimeMillis();
        List<Thread> threads = new ArrayList<>();
        threadInputs.forEach(inputs -> {
            Thread thread = prepareThread(inputs);
            threads.add(thread);
        });

        for (Thread thread : threads) {
            thread.start();
        }

        for (Thread thread : threads) {
            try {
                thread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        System.out.println(System.currentTimeMillis() - start);
    }

    private Thread prepareThread(List<String> inputs) {
        Runnable runnable = () -> {
            inputs.forEach(input -> {
                new LifeChangingApplication(input, personRepository).parseInput();
                System.out.println(input + " parsed");
            });
        };

        return new Thread(runnable);
    }
}
