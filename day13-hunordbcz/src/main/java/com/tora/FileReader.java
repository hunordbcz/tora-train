package com.tora;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.BufferUnderflowException;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;

public class FileReader {
    private FileChannel fileChannel;
    private RandomAccessFile file;
    private MappedByteBuffer buffer;
    private long size;

    public FileReader(String inputFile) {
        try {
            file = new RandomAccessFile(new File(inputFile), "r");
            fileChannel = file.getChannel();
            size = fileChannel.size();
            buffer = fileChannel.map(FileChannel.MapMode.READ_ONLY, 0, fileChannel.size());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String next() throws BufferUnderflowException {
        StringBuilder stringBuilder = new StringBuilder();

        char ch;
        while ((ch = (char) buffer.get()) != '#') {
            stringBuilder.append(ch);
        }

        return stringBuilder.toString();
    }

    public boolean hasNext() {
        return buffer.position() != size;
    }

    public void close() {
        try {
            fileChannel.close();
            file.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
