package com.tora;

import com.tora.exceptions.InvalidInputException;
import com.tora.regex.AbstractValidator;
import com.tora.regex.EmailValidator;
import com.tora.regex.IDValidator;
import com.tora.regex.NameValidator;

public enum InputType {
    NAME(new NameValidator()),
    ID(new IDValidator()),
    EMAIL(new EmailValidator());

    private final AbstractValidator validator;

    InputType(AbstractValidator validator) {
        this.validator = validator;
    }

    public void validate(String expression) throws InvalidInputException {
        if (!validator.test(expression)) {
            throw new InvalidInputException("Expression invalid: " + expression);
        }
    }
}
