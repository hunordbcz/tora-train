package com.tora;

import org.apache.commons.cli.MissingArgumentException;

import java.util.Arrays;
import java.util.List;

public class App {
    public static void main(String[] args) throws MissingArgumentException {
        List<String> inputs = Arrays.asList(
                "day13_input3.txt",
                "day13_input3.txt",
                "day13_input3.txt",
                "day13_input3.txt"
        );

        ThreadManager threadManager = new ThreadManager(4, inputs);
        threadManager.run();


//        LifeChangingApplication app;
//        if (args.length > 0) {
//            app = new LifeChangingApplication(args[0]);
//        } else {
//            throw new MissingArgumentException("Missing input file argument");
//        }
//
//        app.parseInput();
    }
}
