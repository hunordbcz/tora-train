package com.tora;

import java.io.IOException;

public class FileWriter {
    private java.io.FileWriter writer;

    public FileWriter(String fileName) {
        try {
            this.writer = new java.io.FileWriter(fileName);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void write(String text) {
        try {
            writer.write(text);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void writeln(String text) {
        write(text + "\n");
    }

    public void close() {
        try {
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
