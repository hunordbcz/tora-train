package com.tora;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.function.BiPredicate;
import java.util.stream.Stream;

public class IOStream {
    private IOStream() {

    }

    public static Stream<String> getLines(String path) throws IOException {
        Stream.Builder<String> streamBuilder = Stream.builder();
        String line;
        File file = new File(path); //fixme txt or binary ? + FileInputStream change
        if (!file.isFile()) {
            throw new IOException("Specified path is not a file");
        }
        try (BufferedReader br = new BufferedReader(new FileReader(file))) {
            while ((line = br.readLine()) != null) {
                streamBuilder.add(line);
            }
        }

        return streamBuilder.build();
    }

    public static Stream<String> walk(String path) {
        return walk(path, -1);
    }

    public static Stream<String> walk(String path, int maxDepth) {
        return walk(path, maxDepth, (k1, k2) -> true);
    }

    public static Stream<String> walk(String path, int maxDepth, BiPredicate<String, String> predicate) {
        return pathWalkRecursive(path, 1, maxDepth, predicate);
    }

    //fixme Use Path.class instead of String.class at path param
    private static Stream<String> pathWalkRecursive(String path, int currentDepth, int maxDepth, BiPredicate<String, String> predicate) {
        Stream.Builder<String> streamBuilder = Stream.builder();
        File file = new File(path);
        if (!file.exists()) {
            return streamBuilder.build();
        }

        streamBuilder.add(file.getPath());
        for (File fileEntry : file.listFiles()) {

            if (fileEntry.isDirectory() && (maxDepth == -1 || currentDepth < maxDepth)) {
                pathWalkRecursive(fileEntry.getPath(), currentDepth + 1, maxDepth, predicate)
                        .forEach(streamBuilder::add);
            } else if (predicate.test(fileEntry.getPath(), fileEntry.getName())) {
                streamBuilder.add(fileEntry.getPath());
            }
        }

        return streamBuilder.build();
    }
}
