package com.tora;

import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.Fork;
import org.openjdk.jmh.annotations.Measurement;
import org.openjdk.jmh.annotations.Warmup;
import org.openjdk.jmh.infra.Blackhole;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class BenchmarkClass {


    @Benchmark
    @Fork(1)
    @Warmup(iterations = 5, time = 3)
    @Measurement(iterations = 10, time = 3)
    public void walkIO() {
        IOStream.walk(".").forEach(
                o -> Blackhole.consumeCPU(1)
        );
    }

    @Benchmark
    @Fork(1)
    @Warmup(iterations = 5, time = 3)
    @Measurement(iterations = 10, time = 3)
    public void walkNIO() throws IOException {
        Files.walk(Paths.get(".")).forEach(
                o -> Blackhole.consumeCPU(1)
        );
    }

    @Benchmark
    @Fork(1)
    @Warmup(iterations = 5, time = 3)
    @Measurement(iterations = 10, time = 3)
    public void lineIO() throws IOException {
        IOStream.getLines("day13_input3.txt-errors.txt").forEach(
                o -> Blackhole.consumeCPU(1)
        );
    }

    @Benchmark
    @Fork(1)
    @Warmup(iterations = 5, time = 3)
    @Measurement(iterations = 10, time = 3)
    public void lineNIO() throws IOException {
        Files.lines(Paths.get("day13_input3.txt-errors.txt")).forEach(
                o -> Blackhole.consumeCPU(1)
        );
    }
}
