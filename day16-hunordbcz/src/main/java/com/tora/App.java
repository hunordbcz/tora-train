package com.tora;

import java.io.IOException;

public class App {
    public static void main(String[] args) throws IOException {
        IOStream.walk(".").forEach(System.out::println);
    }
}
