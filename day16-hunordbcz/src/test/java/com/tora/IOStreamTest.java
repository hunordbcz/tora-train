package com.tora;

import org.junit.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class IOStreamTest {

    @Test
    public void getLines() throws IOException {
        /*Object[] simple = IOStream.getLines("C:\\Users\\debre\\IdeaProjects\\tora-train\\day13_input3.txt-errors.txt").toArray(); Totally bad
        Object[] originalSimple = Files.lines(Paths.get("C:\\Users\\debre\\IdeaProjects\\tora-train\\day13_input3.txt-errors.txt")).toArray();
        assertThat(simple, is(originalSimple));*/
    }

    @Test
    public void find() throws IOException {
        Object[] simple = IOStream.walk(".").toArray();
        Object[] originalSimple = Files.find(Paths.get("."), 10, (a, b) -> true).map(Path::toString).toArray();

        Object[] depth = IOStream.walk(".", 1).toArray();
        Object[] originalDepth = Files.find(Paths.get("."), 1, (a, b) -> true).map(Path::toString).toArray();
        assertThat(simple, is(originalSimple));
        assertThat(depth, is(originalDepth));
    }
}