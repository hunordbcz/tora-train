package com.tora.database;

import com.tora.model.user.HasID;
import org.hibernate.*;
import org.hibernate.criterion.Example;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;

public class TableHandler extends Observable {
    SessionFactory factory = RepositoryHandler.getInstance();
    Transaction tx;

    private TableHandler() {

    }

    public static TableHandler getInstance() {
        return Singleton.tableHandler;
    }

    public <T extends HasID> int add(T obj) {
        int ID = 0;

        try (Session session = factory.openSession()) {
            tx = session.beginTransaction();
            ID = (int) session.save(obj);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
        } finally {
            tx = null;
        }

        refresh(obj.getClass());
        return ID;
    }

    public List all(String table) {
        List objects = null;

        try (Session session = factory.openSession()) {
            tx = session.beginTransaction();
            objects = session.createQuery("FROM " + table).list();
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
        } finally {
            tx = null;
        }

        if (objects == null) {
            return new ArrayList<>();
        }

        return objects;
    }

    public <T extends HasID> T find(Class<T> type, int ID) {
        T obj = null;

        try (Session session = factory.openSession()) {
            tx = session.beginTransaction();
            obj = (T) session.get(type, ID);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
        } finally {
            tx = null;
        }

        return obj;
    }

    public <T extends HasID> List where(T obj) {
        Session session = factory.openSession();
        Example addressExample = Example.create(obj);
        Criteria criteria = session.createCriteria(obj.getClass()).add(addressExample);
        return criteria.list();
    }

    public <T extends HasID> boolean update(T obj) {
        try (Session session = factory.openSession()) {
            tx = session.beginTransaction();
            session.update(obj);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) tx.rollback();
            return false;
        } finally {
            tx = null;
        }

        refresh(obj.getClass());
        return true;
    }

    public <T extends HasID> boolean delete(Class<T> type, int ID) {
        T obj;

        try (Session session = factory.openSession()) {
            tx = session.beginTransaction();
            obj = session.get(type, ID);
            session.delete(obj);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) tx.rollback();
            return false;
        } finally {
            tx = null;
        }

        refresh(obj.getClass());
        return true;
    }

    public <T extends HasID> boolean delete(T obj) {
        return this.delete(obj.getClass(), obj.getId());
    }

    public <T extends HasID> void refresh(Class<T> type) {
        setChanged();
        notifyObservers(type);
    }

    private static class Singleton {
        private static final TableHandler tableHandler = new TableHandler();
    }
}
