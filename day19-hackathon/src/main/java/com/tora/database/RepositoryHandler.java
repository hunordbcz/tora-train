package com.tora.database;


import com.tora.model.user.User;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import java.io.File;

public class RepositoryHandler {

    public static SessionFactory getInstance() {
        return RepositoryHandler.DBHandler.FACTORY;
    }

    private static class DBHandler {
        private static final SessionFactory FACTORY = new Configuration()
                .configure(new File("day19-hackathon/hibernate.cfg.xml"))
                .addAnnotatedClass(User.class)
                .buildSessionFactory();
    }
}
