package com.tora.helper;

import com.tora.config.Role;
import com.tora.model.user.*;

public class UserFactory {
    private UserFactory() {

    }

    public static User getUser(String name, String email, String password, Role role) {
        switch (role) {
            case ADMINISTRATOR:
                return new Administrator(name, email, password);
            case DEVELOPER:
                return new Developer(name, email, password);
            case HUMAN_RESOURCES:
                return new HumanResources(name, email, password);
            case QUALITY_ASSURANCE:
                return new QualityAssurance(name, email, password);
            default:
                return null;
        }
    }
}
