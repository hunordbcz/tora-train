package com.tora.helper;

import com.tora.config.Env;
import org.apache.commons.codec.digest.HmacUtils;

import static org.apache.commons.codec.digest.HmacAlgorithms.HMAC_SHA_512;

public final class Hash {
    private Hash() {

    }

    public static String make(String text) {
        return new HmacUtils(HMAC_SHA_512, Env.get("APP_KEY")).hmacHex(text);
    }
}
