package com.tora.config;

import java.util.Map;

import static java.util.Map.entry;

public class Env {
    static final Map<String, String> envVariables;

    static {
        envVariables = Map.ofEntries(
                entry("APP_KEY", "9b61b771-f9b6-48fa-bf59-176c3d0a6050")
        );
    }

    private Env() {

    }

    public static String get(String key) {
        return envVariables.get(key);
    }
}
