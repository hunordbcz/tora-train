package com.tora.config;

import com.tora.ui.*;

public enum Route {
    LOGIN("Login", new LoginUI()),
    ADD_OFFICE("Add Office", new AddOfficeUI()),
    ADD_EMPLOYEE("Add Employee", new AddEmployeeUI()),
    EMPLOYEE("Employee Dashboard", new EmployeeUI()),
    ADMINISTRATOR("Administrator Dashboard", new AdminUI()),
    VIEW_RESERVATION("View Reservations", new ViewReservationsUI()),
    MAKE_RESERVATION("Make Reservation", new MakeReservationUI());

    private final AbstractJFrame jFrame;
    private final String title;

    Route(String title, AbstractJFrame jFrame) {
        this.title = title;
        this.jFrame = jFrame;
    }

    public static Route getRoute(String routeName) {
        for (Route route : Route.values()) {
            if (route.getTitle().equals(routeName)) {
                return route;
            }
        }

        return null;
    }

    public String getTitle() {
        return title;
    }

    public AbstractJFrame getjFrame() {
        return jFrame;
    }

    public void setArgs(Object[] args) {
        this.jFrame.setArgs(args);
    }
}
