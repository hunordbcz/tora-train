package com.tora.config;

public enum Role {
    ADMINISTRATOR("Administrator", "Administrator Dashboard"),
    DEVELOPER("Developer", "Employee Dashboard"),
    QUALITY_ASSURANCE("Quality Assurance", "Employee Dashboard"),
    HUMAN_RESOURCES("Human Resources", "Employee Dashboard");

    private final String name;
    private final String homeRoute;

    Role(String name, String homeRoute) {
        this.name = name;
        this.homeRoute = homeRoute;
    }

    public static Role getRole(String roleName) {
        for (Role role : Role.values()) {
            if (role.getName().equals(roleName)) {
                return role;
            }
        }

        return null;
    }

    public String getName() {
        return this.name;
    }

    public Route getHome() {
        return Route.getRoute(homeRoute);
    }

    @Override
    public String toString() {
        return this.getName();
    }
}
