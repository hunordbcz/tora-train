package com.tora.validator;

public class NameValidator extends RegexValidator {

    @Override
    protected String getRegex() {
        return "[a-zA-Z]+|-";
    }
}