package com.tora.validator;

public class EmailValidator extends RegexValidator {

    @Override
    protected String getRegex() {
        return "^[\\w!#$%&'*+/=?`{|}~^-]+(?:\\.[\\w!#$%&'*+/=?`{|}~^-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$";
    }
}
