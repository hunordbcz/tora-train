package com.tora.validator;

import java.util.function.Predicate;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public abstract class RegexValidator implements Predicate<String> {
    @Override
    public boolean test(String s) {
        Pattern p = Pattern.compile(getRegex());
        Matcher m = p.matcher(s);
        return m.matches();
    }

    protected abstract String getRegex();
}