package com.tora.ui;

import com.tora.config.Route;
import com.tora.controller.auth.RegisterController;
import com.tora.database.TableHandler;
import com.tora.model.Office;
import com.tora.model.user.HasReservation;
import com.tora.ui.tables.EmployeeTableModel;
import com.tora.ui.tables.OfficeTableModel;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

public class AdminUI extends AbstractJFrame implements Observer {
    private JTabbedPane tabbedPane;
    private JPanel adminPanel;
    private JPanel homeLink;
    private JPanel employeesLink;
    private JPanel officeSpacesLink;
    private JPanel rulesLink;
    private JButton logOutButton;
    private JTable employeeTable;
    private JButton addEmployeeButton;
    private JButton removeSelectedButton;
    private JButton viewUserReservationsButton;
    private JButton addOfficeButton;
    private JButton removeOfficeButton;
    private JTable officeTable;
    private JButton viewOfficeReservationsButton;
    private final TableHandler tableHandler;
    private EmployeeTableModel employeeTableModel;
    private OfficeTableModel officeTableModel;

    public AdminUI() {
        RegisterController registerController = new RegisterController();
        tableHandler = TableHandler.getInstance();

        this.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                router.confirm("Are you sure you want to exit ?", () -> System.exit(0), null);
            }
        });

        logOutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                router.confirm("Are you sure you want to log out ?", registerController::logout, null);
            }
        });
        employeeListeners();
        officeListeners();
    }

    @Override
    public JPanel getPanel() {
        return adminPanel;
    }

    public void officeListeners() {
        List<Office> officeList = (List<Office>) tableHandler.all("offices");
        officeTableModel = new OfficeTableModel(officeList);
        officeTable.setAutoCreateColumnsFromModel(true);
        officeTable.setModel(officeTableModel);
        officeTable.getTableHeader().setReorderingAllowed(false);
        ListSelectionModel officeTableSelectionModel = officeTable.getSelectionModel();


        officeTableSelectionModel.addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                if (e.getValueIsAdjusting()) {
                    return;
                }
                removeOfficeButton.setEnabled(true);
                viewOfficeReservationsButton.setEnabled(true);
            }
        });

        addOfficeButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                router.redirect(Route.ADD_OFFICE);
            }
        });

        removeOfficeButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int selectedRow = officeTable.getSelectedRow();
                if (selectedRow < 0) {
                    return;
                }

                officeTableModel.removeItem(selectedRow);
                officeTable.clearSelection();
                removeOfficeButton.setEnabled(false);
                viewOfficeReservationsButton.setEnabled(false);
            }
        });

        viewOfficeReservationsButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                HasReservation owner = officeTableModel.getItem(officeTable.getSelectedRow());
                router.redirect(Route.VIEW_RESERVATION, owner);
            }
        });
    }

    public void employeeListeners() {
        employeeTableModel = new EmployeeTableModel(tableHandler.all("users"));
        employeeTable.setAutoCreateColumnsFromModel(true);
        employeeTable.setModel(employeeTableModel);
        employeeTable.getTableHeader().setReorderingAllowed(false);
        ListSelectionModel employeeSelectionModel = employeeTable.getSelectionModel();

        employeeSelectionModel.addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                if (e.getValueIsAdjusting()) {
                    return;
                }
                removeSelectedButton.setEnabled(true);
                viewUserReservationsButton.setEnabled(true);
            }
        });

        addEmployeeButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                router.redirect(Route.ADD_EMPLOYEE);
            }
        });

        removeSelectedButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int selectedRow = employeeTable.getSelectedRow();
                if (selectedRow < 0) {
                    return;
                }

                employeeTableModel.removeItem(selectedRow);
                employeeTable.clearSelection();
                removeSelectedButton.setEnabled(false);
                viewUserReservationsButton.setEnabled(false);
            }
        });

        viewUserReservationsButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                HasReservation reservable = employeeTableModel.getItem(employeeTable.getSelectedRow());
                router.redirect(Route.VIEW_RESERVATION, reservable);
            }
        });
    }

    @Override
    public void update(Observable o, Object arg) {
        if (employeeTableModel != null) {
            employeeTableModel.refresh();
        }

        if (officeTableModel != null) {
            officeTableModel.refresh();
        }
    }
}
