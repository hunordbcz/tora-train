package com.tora.ui;

import com.tora.config.Role;
import com.tora.controller.auth.RegisterController;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class AddEmployeeUI extends AbstractJFrame {
    private JButton createButton;
    private JButton clearButton;
    private JComboBox<Role> roleList;
    private JPasswordField password_confirmation;
    private JPasswordField password;
    private JTextField email;
    private JTextField name;
    private JPanel employeeCreatePanel;

    public AddEmployeeUI() {
        RegisterController registerController = new RegisterController();
        for (Role role : Role.values()) {
            roleList.addItem(role);
        }

        this.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                router.confirm("Are you sure you want to go back ?", router::back, null);
            }
        });

        clearButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

            }
        });
        clearButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                name.setText("");
                email.setText("");
                password.setText("");
                password_confirmation.setText("");
            }
        });
        createButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                registerController.register(name.getText(), email.getText(), password.getPassword(), password_confirmation.getPassword(), (Role) roleList.getSelectedItem());
            }
        });
    }

    @Override
    public JPanel getPanel() {
        return employeeCreatePanel;
    }
}
