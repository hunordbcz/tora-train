package com.tora.ui;

import com.tora.controller.OfficeController;

import javax.swing.*;
import javax.swing.text.DefaultFormatterFactory;
import javax.swing.text.NumberFormatter;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.text.NumberFormat;

public class AddOfficeUI extends AbstractJFrame {
    private JPanel officeCreatePanel;
    private final OfficeController officeController;
    private JTextField officeName;
    private JFormattedTextField nrSpaces;
    private JButton createButton;
    private JButton clearButton;

    public AddOfficeUI() {
        officeController = new OfficeController();

        this.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                router.confirm("Are you sure you want to go back ?", router::back, null);
            }
        });

        NumberFormat nf = NumberFormat.getIntegerInstance();
        NumberFormatter nff = new NumberFormatter(nf);
        DefaultFormatterFactory factory = new DefaultFormatterFactory(nff);
        nrSpaces.setFormatterFactory(factory);
        createButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                officeController.create(officeName.getText(), nrSpaces.getText());
            }
        });

        clearButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                officeName.setText("");
                nrSpaces.setText("");
            }
        });
    }

    @Override
    public JPanel getPanel() {
        return officeCreatePanel;
    }
}
