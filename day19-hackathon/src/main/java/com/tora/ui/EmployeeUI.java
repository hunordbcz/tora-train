package com.tora.ui;

import com.tora.config.Route;
import com.tora.controller.BaseController;
import com.tora.controller.auth.AuthController;
import com.tora.controller.auth.LoginController;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class EmployeeUI extends AbstractJFrame {
    private JPanel employeePanel;
    private JButton logOutButton;
    private JButton viewReservationsButton;

    public EmployeeUI() {
        AuthController authController = new LoginController();

        this.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                router.confirm("Are you sure you want to exit ?", () -> System.exit(0), null);
            }
        });

        logOutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                router.confirm("Are you sure you want to log out ?", authController::logout, null);
            }
        });

        viewReservationsButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                router.redirect(Route.VIEW_RESERVATION, BaseController.state.getUser());
            }
        });
    }

    @Override
    public JPanel getPanel() {
        return employeePanel;
    }
}
