package com.tora.ui;

import com.tora.controller.ReservationController;
import com.tora.database.TableHandler;
import com.tora.model.Office;
import com.tora.model.user.HasReservation;
import com.tora.model.user.User;

import javax.swing.*;
import javax.swing.text.DateFormatter;
import javax.swing.text.DefaultFormatterFactory;
import javax.swing.text.NumberFormatter;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class MakeReservationUI extends AbstractJFrame {
    private JButton createButton;
    private JButton clearButton;
    private JFormattedTextField date;
    private JFormattedTextField nrSpaces;
    private JPanel makeReservationPanel;
    private JComboBox<User> userJComboBox;
    private JComboBox<Office> officeJComboBox;
    private User owner;
    private Office office;
    private ReservationController reservationController = new ReservationController();

    public MakeReservationUI() {
        DateFormat format = new SimpleDateFormat("dd-MM-yyyy");
        DateFormatter formatter = new DateFormatter(format);
        DefaultFormatterFactory factory = new DefaultFormatterFactory(formatter);
        date.setFormatterFactory(factory);
        date.setText(format.format(new Date()));

        NumberFormat nf = NumberFormat.getNumberInstance();
        NumberFormatter nff = new NumberFormatter(nf);
        DefaultFormatterFactory factoryNr = new DefaultFormatterFactory(nff);
        nrSpaces.setFormatterFactory(factoryNr);


        this.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                router.confirm("Are you sure you want to go back ?", router::back, null);
            }
        });

        createButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                reservationController.create(
                        (User) userJComboBox.getSelectedItem(),
                        (Office) officeJComboBox.getSelectedItem(),
                        nrSpaces.getText(),
                        date.getText()
                );
            }
        });

        clearButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                date.setText(format.format(new Date()));
                nrSpaces.setText("");
            }
        });
    }

    @Override
    public JPanel getPanel() {
        return makeReservationPanel;
    }

    @Override
    public void setArgs(Object[] args) {
        if (args.length == 1) {
            HasReservation obj = (HasReservation) args[0];
            if (args[0] instanceof User) {
                List<Office> officeList = TableHandler.getInstance().all("offices");
                for (Office office : officeList) {
                    this.officeJComboBox.addItem(office);
                }

                owner = (User) obj;
                userJComboBox.removeAllItems();
                userJComboBox.addItem(owner);
                userJComboBox.setEnabled(false);
                officeJComboBox.setEnabled(true);

            } else if (args[0] instanceof Office) {
                List<User> userList = TableHandler.getInstance().all("users");
                for (User user : userList) {
                    this.userJComboBox.addItem(user);
                }

                office = (Office) obj;
                officeJComboBox.removeAllItems();
                officeJComboBox.addItem(office);
                officeJComboBox.setEnabled(false);
                userJComboBox.setEnabled(true);
            }
            this.setTitle("New " + obj.getClass().getSimpleName() + " Reservation - " + obj.getId());
        } else {
            super.setArgs(args);
        }
    }
}
