package com.tora.ui;

import com.tora.config.Route;
import com.tora.controller.BaseController;
import com.tora.model.Reservation;
import com.tora.model.user.HasReservation;
import com.tora.ui.tables.ReservationTableModel;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

public class ViewReservationsUI extends AbstractJFrame implements Observer {
    private JScrollPane reservationScrollPane;
    private JTable reservationTable;
    private JButton removeReservationButton;
    private JButton addReservationButton;
    private JPanel reservationsPanel;
    private HasReservation owner;
    private ReservationTableModel reservationTableModel;

    public ViewReservationsUI() {
        this.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                router.confirm("Are you sure you want to go back ?", () -> router.redirect(BaseController.state.getUser().homeRoute()), null);
            }
        });
        addReservationButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                router.redirect(Route.MAKE_RESERVATION, owner);
            }
        });
        removeReservationButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

            }
        });
    }

    @Override
    public JPanel getPanel() {
        return reservationsPanel;
    }

    @Override
    public void setArgs(Object[] args) {
        if (args.length == 1) {
            owner = (HasReservation) args[0];
            List<Reservation> reservationList = owner.getReservations();

            reservationTableModel = new ReservationTableModel(owner, reservationList);
            reservationTable.setAutoCreateColumnsFromModel(true);
            reservationTable.setModel(reservationTableModel);
            reservationTable.getTableHeader().setReorderingAllowed(false);

            this.setTitle(owner.getClass().getSimpleName() + " Reservations - " + owner.getId());
        } else {
            super.setArgs(args);
        }
    }

    @Override
    public void update(Observable o, Object arg) {
        if (reservationTableModel != null) {
            reservationTableModel.refresh();
        }
    }
}
