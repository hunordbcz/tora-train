package com.tora.ui;

import com.tora.controller.auth.LoginController;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class LoginUI extends AbstractJFrame {
    private final LoginController loginController;
    private JButton loginButton;
    private JPasswordField passwordField;
    private JTextField textField;
    private JPanel loginPanel;

    public LoginUI() {
        loginController = new LoginController();

        this.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                router.confirm("Are you sure you want to exit ?", () -> System.exit(0), null);
            }
        });

        loginButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                loginController.login(textField.getText(), passwordField.getPassword());
            }
        });
    }

    public JPanel getPanel() {
        return loginPanel;
    }
}
