package com.tora.ui.tables;

import com.tora.database.TableHandler;
import com.tora.model.Office;
import com.tora.model.Reservation;
import com.tora.model.user.HasReservation;

import javax.swing.table.AbstractTableModel;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class ReservationTableModel extends AbstractTableModel {

    private List<Reservation> source;
    private HasReservation owner;

    public ReservationTableModel(HasReservation owner, List<Reservation> src) {
        this.owner = owner;
        this.source = Objects.requireNonNullElseGet(src, ArrayList::new);
    }

    @Override
    public int getRowCount() {
        return source.size();
    }

    @Override
    public int getColumnCount() {
        return 3;
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        switch (columnIndex) {
            case 0:
                return Integer.class;
            case 1:
            case 2:
                return String.class;
        }

        return null;
    }

    @Override
    public String getColumnName(int columnIndex) {
        switch (columnIndex) {
            case 0:
                return "ID";
            case 1:
                return "USER";
            case 2:
                return "DAY";
        }

        return null;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Reservation reservation = source.get(rowIndex);
        switch (columnIndex) {
            case 0:
                return reservation.getId();
            case 1:
                return reservation.getUser().getName();
            case 2:
                DateFormat format = new SimpleDateFormat("dd-MM-yyyy");
                return format.format(reservation.getDate());
        }
        return null;
    }

    public void removeItem(int row) {
        TableHandler.getInstance().delete(source.get(row));
    }

    public void refresh() {
        HasReservation owner = TableHandler.getInstance().find(Office.class, this.owner.getId());
        if (owner != null) {
            this.source = owner.getReservations();
        } else {
            this.source = new ArrayList<>();
        }

        fireTableRowsUpdated(0, getRowCount());
    }
}
