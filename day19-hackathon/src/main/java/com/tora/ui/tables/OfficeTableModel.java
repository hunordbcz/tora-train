package com.tora.ui.tables;

import com.tora.controller.OfficeController;
import com.tora.database.TableHandler;
import com.tora.model.Office;
import com.tora.model.Reservation;
import com.tora.model.user.HasReservation;

import javax.swing.table.AbstractTableModel;
import java.util.List;

public class OfficeTableModel extends AbstractTableModel {
    private List<Office> source;
    private final OfficeController officeController;

    public OfficeTableModel(List<Office> src) {
        this.officeController = new OfficeController();
        this.source = src;
    }

    @Override
    public int getRowCount() {
        return source.size();
    }

    @Override
    public int getColumnCount() {
        return 4;
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        switch (columnIndex) {
            case 0:
            case 3:
                return Integer.class;
            case 1:
                return String.class;
            case 2:
                return Long.class;
        }

        return null;
    }

    @Override
    public String getColumnName(int columnIndex) {
        switch (columnIndex) {
            case 0:
                return "ID";
            case 1:
                return "NAME";
            case 2:
                return "NR SPACES";
            case 3:
                return "RESERVATIONS";
        }

        return null;
    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        Office office = source.get(rowIndex);
        switch (columnIndex) {
            case 1:
                office.setName((String) aValue);
                break;
            case 2:
                office.setSpaces((Long) aValue);
                break;
        }

        officeController.update(office);
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return columnIndex == 1 || columnIndex == 2;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Office office = source.get(rowIndex);
        switch (columnIndex) {
            case 0:
                return office.getId();
            case 1:
                return office.getName();
            case 2:
                return office.getSpaces();
            case 3:
                List<Reservation> reservations = office.getReservations();
                return reservations.size();
        }
        return null;
    }

    public void removeItem(int row) {
        TableHandler.getInstance().delete(source.get(row));
    }

    public HasReservation getItem(int index) {
        return source.get(index);
    }

    public void refresh() {
        this.source = (List<Office>) TableHandler.getInstance().all("offices");
        fireTableRowsUpdated(0, getRowCount());
    }
}
