package com.tora.ui.tables;

import com.tora.controller.BaseController;
import com.tora.controller.RouteController;
import com.tora.database.TableHandler;
import com.tora.model.Reservation;
import com.tora.model.user.HasReservation;
import com.tora.model.user.User;

import javax.swing.*;
import javax.swing.table.AbstractTableModel;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class EmployeeTableModel extends AbstractTableModel {

    private List<User> source;

    public EmployeeTableModel(List<User> src) {
        this.source = Objects.requireNonNullElse(src, new ArrayList<>());
    }

    @Override
    public int getRowCount() {
        return source.size();
    }

    @Override
    public int getColumnCount() {
        return 5;
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        switch (columnIndex) {
            case 0:
            case 3:
                return Integer.class;
            case 1:
            case 2:
            case 4:
                return String.class;
        }

        return null;
    }

    @Override
    public String getColumnName(int columnIndex) {
        switch (columnIndex) {
            case 0:
                return "ID";
            case 1:
                return "NAME";
            case 2:
                return "EMAIL";
            case 3:
                return "RESERVATIONS";
            case 4:
                return "ROLE";
        }

        return null;
    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        User user = source.get(rowIndex);
        switch (columnIndex) {
            case 1:
                user.setName((String) aValue);
                break;
            case 2:
                user.setEmail((String) aValue);
                break;
        }

        TableHandler.getInstance().update(user);
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return columnIndex == 1 || columnIndex == 2;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        User user = source.get(rowIndex);
        switch (columnIndex) {
            case 0:
                return user.getId();
            case 1:
                return user.getName();
            case 2:
                return user.getEmail();
            case 3:
                List<Reservation> reservations = user.getReservations();
                return reservations.size();
            case 4:
                return user.getRole().getName();
        }
        return null;
    }

    public void removeItem(int row) {
        User user = source.get(row);
        if (user.getId() == BaseController.state.getUser().getId()) {
            RouteController.getInstance().popup("You can't delete yourself", JOptionPane.ERROR_MESSAGE);
            return;
        }

        TableHandler tableHandler = TableHandler.getInstance();
        user.getReservations().forEach(tableHandler::delete);
        tableHandler.delete(user);
    }

    public void refresh() {
        this.source = (List<User>) TableHandler.getInstance().all("users");
        fireTableRowsUpdated(0, getRowCount());
    }

    public HasReservation getItem(int index) {
        return source.get(index);
    }
}
