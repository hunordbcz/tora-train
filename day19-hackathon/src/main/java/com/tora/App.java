package com.tora;

import com.tora.config.Route;
import com.tora.controller.RouteController;
import com.tora.database.TableHandler;
import com.tora.ui.AdminUI;
import com.tora.ui.ViewReservationsUI;


public class App {
    public static void main(String[] args) {
        RouteController router = RouteController.getInstance();
        router.redirect(Route.LOGIN);

        TableHandler tableHandler = TableHandler.getInstance();
        tableHandler.addObserver((AdminUI) Route.ADMINISTRATOR.getjFrame());
        tableHandler.addObserver((ViewReservationsUI) Route.VIEW_RESERVATION.getjFrame());


//        User user = tableHandler.find(User.class, 36);
//        Office office = tableHandler.find(Office.class, 1);
//        Reservation reservation = new Reservation(user,office);
//
//        tableHandler.add(reservation);
//        List users = tableHandler.all("users");
//        User user = (User)users.get(0);
//        Reservation reservation = new Reservation();
//        Office office = new Office("test", 2);
//        reservation.setUser(user);
//        reservation.setOffice(office);
//        user.addReservation(reservation);
//        tableHandler.add(office);
//        tableHandler.add(reservation);
//        tableHandler.update(user);
//        System.out.println("test");
    }
}
