package com.tora.model;

import com.tora.config.Route;
import com.tora.controller.RouteController;
import com.tora.model.user.User;

public class State {
    private User user;

    public User getUser() {
        if (user == null) {
            RouteController.getInstance().redirect(Route.LOGIN);
            return null;
        }

        try {
            return (User) user.clone();
        } catch (CloneNotSupportedException e) {
            return user;
        }
    }

    public void setUser(User user) {
        this.user = user;
    }
}
