package com.tora.model.user;

import com.tora.config.Role;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("ADMINISTRATOR")
public final class Administrator extends User {

    public Administrator(String name, String email, String password) {
        super(name, email, password);
    }

    public Administrator() {
        super();
    }

    public Role getRole() {
        return Role.ADMINISTRATOR;
    }
}
