package com.tora.model.user;

import com.tora.model.Reservation;

import java.util.List;

public interface HasReservation extends HasID {
    public List<Reservation> getReservations();

    public void setReservations(List<Reservation> reservations);

    public void addReservation(Reservation reservation);
}
