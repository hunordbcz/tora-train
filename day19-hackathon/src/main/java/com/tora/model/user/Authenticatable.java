package com.tora.model.user;

import javax.persistence.*;

@Entity(name = "users")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "role",
        discriminatorType = DiscriminatorType.STRING)
@DiscriminatorValue("null")
public class Authenticatable extends HasIDImpl {
    private String email;
    private String password;

    public Authenticatable(String email, String password) {
        this.email = email;
        this.password = password;
    }

    public Authenticatable() {

    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public Authenticatable setEmail(String email) {
        this.email = email;
        return this;
    }

    public Authenticatable setPassword(String password) {
        this.password = password;
        return this;
    }
}
