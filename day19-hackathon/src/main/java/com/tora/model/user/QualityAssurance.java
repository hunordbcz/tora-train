package com.tora.model.user;

import com.tora.config.Role;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("QUALITY_ASSURANCE")
public final class QualityAssurance extends User {
    public QualityAssurance(String name, String email, String password) {
        super(name, email, password);
    }

    public QualityAssurance() {
        super();
    }

    public Role getRole() {
        return Role.QUALITY_ASSURANCE;
    }

}
