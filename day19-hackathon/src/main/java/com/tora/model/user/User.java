package com.tora.model.user;

import com.tora.config.Role;
import com.tora.config.Route;
import com.tora.model.Reservation;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import java.util.ArrayList;
import java.util.List;

@Entity
public abstract class User extends Authenticatable implements Cloneable, HasReservation {
    @Column(name = "name")
    private String name;

    @OneToMany(mappedBy = "user", fetch = FetchType.EAGER)
    private List<Reservation> reservations;

    public User(String name, String email, String password) {
        super(email, password);
        this.name = name;
        this.reservations = new ArrayList<>();
    }

    public User() {

    }

    @Override
    public List<Reservation> getReservations() {
        return reservations;
    }

    @Override
    public void setReservations(List<Reservation> reservations) {
        this.reservations = reservations;
    }

    @Override
    public void addReservation(Reservation reservation) {
        this.reservations.add(reservation);
    }

    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public abstract Role getRole();

    public Route homeRoute() {
        return this.getRole().getHome();
    }

    public boolean is(Role role) {
        return getRole() == role;
    }

    @Override
    public String toString() {
        return this.getEmail();
    }
}
