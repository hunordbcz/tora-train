package com.tora.model.user;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import java.util.Objects;

@MappedSuperclass
public abstract class HasIDImpl implements HasID {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    public HasIDImpl() {

    }

    @Override
    public int getId() {
        return id;
    }

    @Override
    public void setId(int id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        HasIDImpl hasID = (HasIDImpl) o;
        return getId() == hasID.getId();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }
}
