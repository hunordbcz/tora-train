package com.tora.model.user;

public interface HasID {
    int getId();

    void setId(int id);
}
