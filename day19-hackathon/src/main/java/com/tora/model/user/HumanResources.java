package com.tora.model.user;

import com.tora.config.Role;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("HUMAN_RESOURCES")
public final class HumanResources extends User {
    public HumanResources(String name, String email, String password) {
        super(name, email, password);
    }

    public HumanResources() {
        super();
    }

    public Role getRole() {
        return Role.HUMAN_RESOURCES;
    }

}