package com.tora.model.user;

import com.tora.config.Role;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("DEVELOPER")
public final class Developer extends User {
    public Developer(String name, String email, String password) {
        super(name, email, password);
    }

    public Developer() {
        super();
    }

    public Role getRole() {
        return Role.DEVELOPER;
    }

}
