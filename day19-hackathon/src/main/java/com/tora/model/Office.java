package com.tora.model;

import com.tora.model.user.HasIDImpl;
import com.tora.model.user.HasReservation;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import java.util.List;

@Entity(name = "offices")
public class Office extends HasIDImpl implements HasReservation {

    private String name;
    private Long spaces;

    @OneToMany(mappedBy = "office", fetch = FetchType.EAGER)
    private List<Reservation> reservations;

    public Office(String name, Long spaces) {
        this.name = name;
        this.spaces = spaces;
    }

    public Office() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getSpaces() {
        return spaces;
    }

    public void setSpaces(Long spaces) {
        this.spaces = spaces;
    }

    @Override
    public List<Reservation> getReservations() {
        return reservations;
    }

    @Override
    public void setReservations(List<Reservation> reservations) {
        this.reservations = reservations;
    }

    @Override
    public void addReservation(Reservation reservation) {
        this.reservations.add(reservation);
    }

    @Override
    public String toString() {
        return this.getName();
    }
}
