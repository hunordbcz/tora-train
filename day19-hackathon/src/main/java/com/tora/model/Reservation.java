package com.tora.model;

import com.tora.model.user.HasIDImpl;
import com.tora.model.user.User;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.util.Date;

@Entity(name = "reservations")
public class Reservation extends HasIDImpl {

    @ManyToOne
    @JoinColumn(name = "user_id", nullable = false)
    private User user;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "office_id", nullable = false)
    private Office office;
    private Date date;
    private long spaces;

    public Reservation(User user, Office office) {
        this.user = user;
        this.office = office;
    }

    public Reservation() {

    }

    public Reservation(User user, Office office, long nrSpaces, Date date) {
        this.user = user;
        this.office = office;
        this.spaces = nrSpaces;
        this.date = date;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Office getOffice() {
        return office;
    }

    public void setOffice(Office office) {
        this.office = office;
    }

    public long getSpaces() {
        return spaces;
    }

    public void setSpaces(long spaces) {
        this.spaces = spaces;
    }
}
