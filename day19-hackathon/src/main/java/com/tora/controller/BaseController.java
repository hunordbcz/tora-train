package com.tora.controller;

import com.tora.database.TableHandler;
import com.tora.model.State;

public class BaseController {
    public static final State state = new State();
    public static final RouteController router = RouteController.getInstance();
    public static final TableHandler tableHandler = TableHandler.getInstance();
}
