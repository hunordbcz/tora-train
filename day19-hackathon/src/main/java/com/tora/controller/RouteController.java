package com.tora.controller;

import com.tora.config.Route;
import com.tora.ui.AbstractJFrame;

import javax.swing.*;

public class RouteController {
    private Route previousRoute;
    private Route currentRoute;

    private RouteController() {
        currentRoute = null;
    }

    public void redirect(String routeName) {
        redirect(Route.getRoute(routeName));
    }

    public static RouteController getInstance() {
        return Singleton.INSTANCE;
    }

    public void redirect(Route nextRoute, Object... args) {
        AbstractJFrame window = nextRoute.getjFrame();
        window.setTitle(nextRoute.getTitle());
        window.setContentPane(window.getPanel());
        window.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
        window.pack();

        if (currentRoute == null) {
            window.setLocationRelativeTo(null);
        } else {
            window.setLocation(currentRoute.getjFrame().getX(), currentRoute.getjFrame().getY());
            previousRoute = currentRoute;
            currentRoute.getjFrame().dispose();
        }

        if (args.length != 0) {
            nextRoute.setArgs(args);
        }

        window.setVisible(true);
        currentRoute = nextRoute;
    }

    public void back() {
        if (previousRoute != null && previousRoute != currentRoute) {
            redirect(previousRoute);
            return;
        }
        System.err.println("Previous route either doesn't exist or is the same as the current one");
    }

    public void popup(String message, int type) {
        JOptionPane.showMessageDialog(currentRoute.getjFrame(), message, currentRoute.getTitle() + " - Notification", type);
    }

    public int confirm(String question, Runnable yes, Runnable no) {
        int dialogResult = JOptionPane.showConfirmDialog(null, question, currentRoute.getTitle() + " - Confirm", JOptionPane.YES_NO_OPTION);
        if (dialogResult == JOptionPane.YES_OPTION) {
            yes.run();
        } else if (no != null) {
            no.run();
        }

        return dialogResult;
    }

    private static class Singleton {
        private static final RouteController INSTANCE = new RouteController();
    }
}
