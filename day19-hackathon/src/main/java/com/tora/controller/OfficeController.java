package com.tora.controller;

import com.tora.config.Role;
import com.tora.config.Route;
import com.tora.controller.auth.AuthController;
import com.tora.model.Office;
import com.tora.model.Reservation;
import com.tora.validator.NameValidator;

import javax.swing.*;
import java.util.Date;
import java.util.List;

public class OfficeController extends BaseController {
    public void create(String name, String nrSpacesString) {
        if (!AuthController.check()) {
            router.redirect(Route.LOGIN);
            router.popup("You must be logged in to create a new office", JOptionPane.ERROR_MESSAGE);
            return;
        }

        if (!state.getUser().is(Role.ADMINISTRATOR)) {
            router.redirect(Route.EMPLOYEE);
            router.popup("Unauthorized action", JOptionPane.ERROR_MESSAGE);
            return;
        }

        long nrSpaces = nrSpacesString.isEmpty() ? 0L : Long.parseLong(nrSpacesString);
        String error = validate(name, nrSpaces);
        if (error != null) {
            router.popup(error, JOptionPane.ERROR_MESSAGE);
            return;
        }

        if (!attempt(name, nrSpaces)) {
            router.popup("Office already exists", JOptionPane.ERROR_MESSAGE);
            return;
        }

        router.back();
    }

    private boolean attempt(String name, long nrSpaces) {
        Office office = new Office();
        office.setName(name);
        List offices = tableHandler.where(office);

        if (offices.size() == 0) {
            office.setSpaces(nrSpaces);
            tableHandler.add(office);
            return true;
        }

        return false;
    }

    private String validate(String name, long nrSpaces) {
        NameValidator nameValidator = new NameValidator();
        if (!nameValidator.test(name)) {
            return "Invalid name";
        }

        if (nrSpaces < 1) {
            return "Invalid number of spaces";
        }

        return null;
    }

    public void update(Office office) {
        String error = validate(office.getName(), office.getSpaces());
        if (error != null) {
            router.popup(error, JOptionPane.ERROR_MESSAGE);
            tableHandler.refresh(Office.class);
            return;
        }

        tableHandler.update(office);
    }

    public void delete(Office office) {
        for (Reservation reservation : office.getReservations()) {
            if (reservation.getDate().compareTo(new Date()) > 0) {
                router.popup("There are active reservations, can't delete", JOptionPane.ERROR_MESSAGE);
                tableHandler.refresh(Office.class);
                return;
            }
        }

        office.getReservations().forEach(tableHandler::delete);
        tableHandler.delete(office);
    }
}
