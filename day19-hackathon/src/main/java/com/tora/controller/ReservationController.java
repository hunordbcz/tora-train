package com.tora.controller;

import com.tora.config.Role;
import com.tora.config.Route;
import com.tora.controller.auth.AuthController;
import com.tora.model.Office;
import com.tora.model.Reservation;
import com.tora.model.user.User;

import javax.swing.*;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ReservationController extends BaseController {
    public void create(User user, Office office, String nrSpacesString, String dateString) {
        if (!AuthController.check()) {
            router.redirect(Route.LOGIN);
            router.popup("You must be logged in to create a new reservation", JOptionPane.ERROR_MESSAGE);
            return;
        }

        User currentUser = state.getUser();
        if (!user.equals(currentUser) && !currentUser.is(Role.ADMINISTRATOR)) {
            router.redirect(Route.EMPLOYEE);
            router.popup("Unauthorized action", JOptionPane.ERROR_MESSAGE);
            return;
        }

        long nrSpaces = nrSpacesString.isEmpty() ? 0L : Long.parseLong(nrSpacesString);
        Date date;
        try {
            DateFormat format = new SimpleDateFormat("dd-MM-yyyy");
            date = format.parse(dateString);
        } catch (ParseException e) {
            router.popup("Invalid Date format", JOptionPane.ERROR_MESSAGE);
            return;
        }

        String error = validate(nrSpaces, date);
        if (error != null) {
            router.popup(error, JOptionPane.ERROR_MESSAGE);
            return;
        }

        if (!attempt(user, office, nrSpaces, date)) {
            router.popup("An error occurred", JOptionPane.ERROR_MESSAGE);
            return;
        }

        router.back();
    }

    private boolean attempt(User user, Office office, long nrSpaces, Date date) {
        Reservation reservation = new Reservation(user, office, nrSpaces, date);
        return tableHandler.add(reservation) != 0;
    }

    private String validate(long nrSpaces, Date date) {
        if (nrSpaces < 1) {
            return "Invalid number of spaces";
        }

        if (date.compareTo(new Date()) < 0) {
            return "Date must be in the future";
        }

        return null;
    }

    public void update(Reservation reservation) {
        String error = validate(reservation.getSpaces(), reservation.getDate());
        if (error != null) {
            router.popup(error, JOptionPane.ERROR_MESSAGE);
            tableHandler.refresh(Office.class);
            return;
        }

        tableHandler.update(reservation);
    }

    public void delete(Reservation reservation) {
        tableHandler.delete(reservation);
    }
}
