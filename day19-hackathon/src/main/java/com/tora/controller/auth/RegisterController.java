package com.tora.controller.auth;

import com.tora.config.Role;
import com.tora.config.Route;
import com.tora.helper.Hash;
import com.tora.helper.UserFactory;
import com.tora.model.user.Authenticatable;
import com.tora.model.user.User;
import com.tora.validator.NameValidator;

import javax.swing.*;
import java.util.List;

public final class RegisterController extends AuthController {
    public void register(String name, String email, char[] password, char[] passwordConfirmation, Role role) {
        if (!check()) {
            router.redirect(Route.LOGIN);
            router.popup("You must be logged in to create a new account", JOptionPane.ERROR_MESSAGE);
            return;
        }

        if (!state.getUser().is(Role.ADMINISTRATOR)) {
            router.redirect(Route.EMPLOYEE);
            router.popup("Unauthorized action", JOptionPane.ERROR_MESSAGE);
            return;
        }

        String passwordString = new String(password);
        String passwordConfirmationString = new String(passwordConfirmation);
        String error = validate(name, email, passwordString, passwordConfirmationString);
        if (error != null) {
            router.popup(error, JOptionPane.ERROR_MESSAGE);
            return;
        }

        if (!attempt(name, email, Hash.make(passwordString), role)) {
            router.popup("User already exists", JOptionPane.ERROR_MESSAGE);
            return;
        }

        router.back();
    }

    protected boolean attempt(String name, String email, String password, Role role) {
        Authenticatable user = new Authenticatable().setEmail(email);
        List users = tableHandler.where(user);

        if (users.size() == 0) {
            User usertwo = UserFactory.getUser(name, email, password, role);
            tableHandler.add(usertwo);
            return true;
        }

        return false;
    }

    protected String validate(String name, String email, String password, String passwordConfirmation) {
        String error = validate(email, password);
        if (error != null) {
            return error;
        }

        if (!password.equals(passwordConfirmation)) {
            return "Passwords doesn't match";
        }

        NameValidator nameValidator = new NameValidator();
        if (!nameValidator.test(name)) {
            return "Invalid name";
        }

        return null;
    }
}
