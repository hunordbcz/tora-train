package com.tora.controller.auth;

import com.tora.config.Route;
import com.tora.helper.Hash;
import com.tora.model.user.Authenticatable;
import com.tora.model.user.User;

import javax.swing.*;
import java.util.List;
import java.util.Objects;

public final class LoginController extends AuthController {

    public void login(String email, char[] password) {
        if (check()) {
            router.redirect(user().homeRoute());
            return;
        }

        String passwordString = new String(password);
        String error = validate(email, passwordString);
        if (error != null) {
            router.popup(error, JOptionPane.ERROR_MESSAGE);
            return;
        }

        if (!attempt(email, Hash.make(passwordString))) {
            router.popup("Incorrect password or email", JOptionPane.ERROR_MESSAGE);
            return;
        }

        router.redirect(Objects.requireNonNullElse(state.getUser().homeRoute(), Route.EMPLOYEE));
    }

    public boolean attempt(String email, String password) {
        Authenticatable user = new Authenticatable(email, password);
        List users = tableHandler.where(user);

        if (users.size() > 0) {
            state.setUser((User) users.get(0));
            return true;
        }

        return false;
    }
}
