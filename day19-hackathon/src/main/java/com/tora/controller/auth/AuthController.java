package com.tora.controller.auth;

import com.tora.config.Route;
import com.tora.controller.BaseController;
import com.tora.model.user.User;
import com.tora.validator.EmailValidator;
import com.tora.validator.PasswordValidator;

public class AuthController extends BaseController {
    protected AuthController() {

    }

    public User user() {
        return state.getUser();
    }

    public static boolean check() {
        return state.getUser() != null;
    }

    protected String validate(String email, String password) {
        EmailValidator emailValidator = new EmailValidator();
        PasswordValidator passwordValidator = new PasswordValidator();

        if (!emailValidator.test(email)) {
            return "Invalid email";
        }

        if (!passwordValidator.test(password)) {
            return "Invalid password";
        }

        return null;
    }

    public void logout() {
        state.setUser(null);
        router.redirect(Route.LOGIN);
    }

}
