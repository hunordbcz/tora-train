package com.tora;

import com.koloboke.collect.set.hash.HashObjSet;
import com.koloboke.collect.set.hash.HashObjSets;

public class KolobokeSetBasedRepository<T> implements InMemoryRepository<T> {
    private HashObjSet<T> set;

    public KolobokeSetBasedRepository() {
        set = HashObjSets.newMutableSet();
    }

    @Override
    public void add(T object) {
        set.add(object);
    }

    @Override
    public boolean contains(T object) {
        return set.contains(object);
    }

    @Override
    public void remove(T object) {
        set.remove(object);
    }
}
