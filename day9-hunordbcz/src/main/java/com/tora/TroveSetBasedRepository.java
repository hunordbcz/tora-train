package com.tora;

import gnu.trove.set.hash.THashSet;

public class TroveSetBasedRepository<T> implements InMemoryRepository<T> {
    private THashSet<T> set;

    public TroveSetBasedRepository() {
        this.set = new THashSet<>();
    }

    @Override
    public void add(T object) {
        set.add(object);
    }

    @Override
    public boolean contains(T object) {
        return set.contains(object);
    }

    @Override
    public void remove(T object) {
        set.remove(object);
    }
}
