package com.tora;

import org.eclipse.collections.api.factory.Sets;
import org.eclipse.collections.api.set.MutableSet;

public class EclipseSetBasedRepository<T> implements InMemoryRepository<T> {
    private MutableSet<T> set;

    public EclipseSetBasedRepository() {
        this.set = Sets.mutable.empty();
    }

    @Override
    public void add(T object) {
        set.add(object);
    }

    @Override
    public boolean contains(T object) {
        return set.contains(object);
    }

    @Override
    public void remove(T object) {
        set.remove(object);
    }
}
