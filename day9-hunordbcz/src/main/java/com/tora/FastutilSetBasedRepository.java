package com.tora;

import it.unimi.dsi.fastutil.objects.ObjectOpenHashSet;

import java.util.Set;

public class FastutilSetBasedRepository<T> implements InMemoryRepository<T> {
    private Set<T> set;

    public FastutilSetBasedRepository() {
        this.set = new ObjectOpenHashSet<>();
    }

    @Override
    public void add(T object) {
        set.add(object);
    }

    @Override
    public boolean contains(T object) {
        return set.contains(object);
    }

    @Override
    public void remove(T object) {
        set.remove(object);
    }
}
