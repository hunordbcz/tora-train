package com.tora;

import org.eclipse.collections.api.factory.Lists;
import org.eclipse.collections.api.list.MutableList;

public class EclipseListBasedRepository<T> implements InMemoryRepository<T> {
    private final MutableList<T> list;

    public EclipseListBasedRepository() {
        this.list = Lists.mutable.empty();
    }

    @Override
    public void add(T object) {
        list.add(object);
    }

    @Override
    public boolean contains(T object) {
        return list.contains(object);
    }

    @Override
    public void remove(T object) {
        list.remove(object);
    }
}
