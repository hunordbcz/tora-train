/*
 * Copyright (c) 2014, Oracle America, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  * Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 *  * Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 *  * Neither the name of Oracle nor the names of its contributors may be used
 *    to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.sample;

import com.tora.*;
import org.openjdk.jmh.annotations.*;

import java.util.Random;
import java.util.concurrent.TimeUnit;
import java.util.stream.IntStream;

public class MyBenchmark {

    @Benchmark
    @Fork(1)
    @BenchmarkMode(Mode.AverageTime)
    @OutputTimeUnit(TimeUnit.MICROSECONDS)
    @Warmup(iterations = 3, time = 3)
    @Measurement(iterations = 3, time = 3)
    public void arrayListAdd(MyState state) {
        IntStream.range(0, state.size).forEach(i -> {
            Order order = new Order(new Random().nextInt(state.size), i, i);
            state.arrayListBasedRepository.add(order);
        });
    }

    @Benchmark
    @Fork(1)
    @BenchmarkMode(Mode.AverageTime)
    @OutputTimeUnit(TimeUnit.MICROSECONDS)
    @Warmup(iterations = 3, time = 3)
    @Measurement(iterations = 3, time = 3)
    public void hashSetAdd(MyState state) {
        IntStream.range(0, state.size).forEach(i -> {
            Order order = new Order(new Random().nextInt(state.size), i, i);
            state.hashSetBasedRepository.add(order);
        });
    }

    @Benchmark
    @Fork(1)
    @BenchmarkMode(Mode.AverageTime)
    @OutputTimeUnit(TimeUnit.MICROSECONDS)
    @Warmup(iterations = 3, time = 3)
    @Measurement(iterations = 3, time = 3)
    public void treeSetAdd(MyState state) {
        IntStream.range(0, state.size).forEach(i -> {
            Order order = new Order(new Random().nextInt(state.size), i, i);
            state.treeSetBasedRepository.add(order);
        });
    }

    @Benchmark
    @Fork(1)
    @BenchmarkMode(Mode.AverageTime)
    @OutputTimeUnit(TimeUnit.MICROSECONDS)
    @Warmup(iterations = 3, time = 3)
    @Measurement(iterations = 3, time = 3)
    public void eclipseSetAdd(MyState state) {
        IntStream.range(0, state.size).forEach(i -> {
            Order order = new Order(new Random().nextInt(state.size), i, i);
            state.eclipseSetBasedRepository.add(order);
        });
    }

    @Benchmark
    @Fork(1)
    @BenchmarkMode(Mode.AverageTime)
    @OutputTimeUnit(TimeUnit.MICROSECONDS)
    @Warmup(iterations = 3, time = 3)
    @Measurement(iterations = 3, time = 3)
    public void eclipseListAdd(MyState state) {
        IntStream.range(0, state.size).forEach(i -> {
            Order order = new Order(new Random().nextInt(state.size), i, i);
            state.eclipseListBasedRepository.add(order);
        });
    }

    @Benchmark
    @Fork(1)
    @BenchmarkMode(Mode.AverageTime)
    @OutputTimeUnit(TimeUnit.MICROSECONDS)
    @Warmup(iterations = 3, time = 3)
    @Measurement(iterations = 3, time = 3)
    public void fastutilSetAdd(MyState state) {
        IntStream.range(0, state.size).forEach(i -> {
            Order order = new Order(new Random().nextInt(state.size), i, i);
            state.fastutilSetBasedRepository.add(order);
        });
    }

    @Benchmark
    @Fork(1)
    @BenchmarkMode(Mode.AverageTime)
    @OutputTimeUnit(TimeUnit.MICROSECONDS)
    @Warmup(iterations = 3, time = 3)
    @Measurement(iterations = 3, time = 3)
    public void kolobokeSetAdd(MyState state) {
        IntStream.range(0, state.size).forEach(i -> {
            Order order = new Order(new Random().nextInt(state.size), i, i);
            state.kolobokeSetBasedRepository.add(order);
        });
    }

    @Benchmark
    @Fork(1)
    @BenchmarkMode(Mode.AverageTime)
    @OutputTimeUnit(TimeUnit.MICROSECONDS)
    @Warmup(iterations = 3, time = 3)
    @Measurement(iterations = 3, time = 3)
    public void troveSetAdd(MyState state) {
        IntStream.range(0, state.size).forEach(i -> {
            Order order = new Order(new Random().nextInt(state.size), i, i);
            state.troveSetBasedRepository.add(order);
        });
    }

    @Benchmark
    @Fork(1)
    @BenchmarkMode(Mode.AverageTime)
    @OutputTimeUnit(TimeUnit.MICROSECONDS)
    @Warmup(iterations = 3, time = 3)
    @Measurement(iterations = 3, time = 3)
    public void arrayListContains(MyState state) {
        IntStream.range(0, state.size).forEach(i -> {
            Order order = new Order(new Random().nextInt(state.size), i, i);
            state.arrayListBasedRepository.contains(order);
        });
    }

    @Benchmark
    @Fork(1)
    @BenchmarkMode(Mode.AverageTime)
    @OutputTimeUnit(TimeUnit.MICROSECONDS)
    @Warmup(iterations = 3, time = 3)
    @Measurement(iterations = 3, time = 3)
    public void hashSetContains(MyState state) {
        IntStream.range(0, state.size).forEach(i -> {
            Order order = new Order(new Random().nextInt(state.size), i, i);
            state.hashSetBasedRepository.contains(order);
        });
    }

    @Benchmark
    @Fork(1)
    @BenchmarkMode(Mode.AverageTime)
    @OutputTimeUnit(TimeUnit.MICROSECONDS)
    @Warmup(iterations = 3, time = 3)
    @Measurement(iterations = 3, time = 3)
    public void treeSetContains(MyState state) {
        IntStream.range(0, state.size).forEach(i -> {
            Order order = new Order(new Random().nextInt(state.size), i, i);
            state.treeSetBasedRepository.contains(order);
        });
    }

    @Benchmark
    @Fork(1)
    @BenchmarkMode(Mode.AverageTime)
    @OutputTimeUnit(TimeUnit.MICROSECONDS)
    @Warmup(iterations = 3, time = 3)
    @Measurement(iterations = 3, time = 3)
    public void eclipseListContains(MyState state) {
        IntStream.range(0, state.size).forEach(i -> {
            Order order = new Order(new Random().nextInt(state.size), i, i);
            state.eclipseListBasedRepository.contains(order);
        });
    }

    @Benchmark
    @Fork(1)
    @BenchmarkMode(Mode.AverageTime)
    @OutputTimeUnit(TimeUnit.MICROSECONDS)
    @Warmup(iterations = 3, time = 3)
    @Measurement(iterations = 3, time = 3)
    public void eclipseSetContains(MyState state) {
        IntStream.range(0, state.size).forEach(i -> {
            Order order = new Order(new Random().nextInt(state.size), i, i);
            state.eclipseSetBasedRepository.contains(order);
        });
    }

    @Benchmark
    @Fork(1)
    @BenchmarkMode(Mode.AverageTime)
    @OutputTimeUnit(TimeUnit.MICROSECONDS)
    @Warmup(iterations = 3, time = 3)
    @Measurement(iterations = 3, time = 3)
    public void fastutilSetContains(MyState state) {
        IntStream.range(0, state.size).forEach(i -> {
            Order order = new Order(new Random().nextInt(state.size), i, i);
            state.fastutilSetBasedRepository.contains(order);
        });
    }

    @Benchmark
    @Fork(1)
    @BenchmarkMode(Mode.AverageTime)
    @OutputTimeUnit(TimeUnit.MICROSECONDS)
    @Warmup(iterations = 3, time = 3)
    @Measurement(iterations = 3, time = 3)
    public void kolobokeSetContains(MyState state) {
        IntStream.range(0, state.size).forEach(i -> {
            Order order = new Order(new Random().nextInt(state.size), i, i);
            state.kolobokeSetBasedRepository.contains(order);
        });
    }

    @Benchmark
    @Fork(1)
    @BenchmarkMode(Mode.AverageTime)
    @OutputTimeUnit(TimeUnit.MICROSECONDS)
    @Warmup(iterations = 3, time = 3)
    @Measurement(iterations = 3, time = 3)
    public void troveSetContains(MyState state) {
        IntStream.range(0, state.size).forEach(i -> {
            Order order = new Order(new Random().nextInt(state.size), i, i);
            state.troveSetBasedRepository.contains(order);
        });
    }

    @Benchmark
    @Fork(1)
    @BenchmarkMode(Mode.AverageTime)
    @OutputTimeUnit(TimeUnit.MICROSECONDS)
    @Warmup(iterations = 3, time = 3)
    @Measurement(iterations = 3, time = 3)
    public void arrayListRemove(MyState state) {
        IntStream.range(0, state.size).forEach(i -> {
            Order order = new Order(new Random().nextInt(state.size), i, i);
            state.arrayListBasedRepository.remove(order);
        });
    }

    @Benchmark
    @Fork(1)
    @BenchmarkMode(Mode.AverageTime)
    @OutputTimeUnit(TimeUnit.MICROSECONDS)
    @Warmup(iterations = 3, time = 3)
    @Measurement(iterations = 3, time = 3)
    public void hashSetRemove(MyState state) {
        IntStream.range(0, state.size).forEach(i -> {
            Order order = new Order(new Random().nextInt(state.size), i, i);
            state.hashSetBasedRepository.remove(order);
        });
    }

    @Benchmark
    @Fork(1)
    @BenchmarkMode(Mode.AverageTime)
    @OutputTimeUnit(TimeUnit.MICROSECONDS)
    @Warmup(iterations = 3, time = 3)
    @Measurement(iterations = 3, time = 3)
    public void treeSetRemove(MyState state) {
        IntStream.range(0, state.size).forEach(i -> {
            Order order = new Order(new Random().nextInt(state.size), i, i);
            state.treeSetBasedRepository.remove(order);
        });
    }

    @Benchmark
    @Fork(1)
    @BenchmarkMode(Mode.AverageTime)
    @OutputTimeUnit(TimeUnit.MICROSECONDS)
    @Warmup(iterations = 3, time = 3)
    @Measurement(iterations = 3, time = 3)
    public void eclipseListRemove(MyState state) {
        IntStream.range(0, state.size).forEach(i -> {
            Order order = new Order(new Random().nextInt(state.size), i, i);
            state.eclipseListBasedRepository.remove(order);
        });
    }

    @Benchmark
    @Fork(1)
    @BenchmarkMode(Mode.AverageTime)
    @OutputTimeUnit(TimeUnit.MICROSECONDS)
    @Warmup(iterations = 3, time = 3)
    @Measurement(iterations = 3, time = 3)
    public void eclipseSetRemove(MyState state) {
        IntStream.range(0, state.size).forEach(i -> {
            Order order = new Order(new Random().nextInt(state.size), i, i);
            state.eclipseSetBasedRepository.remove(order);
        });
    }

    @Benchmark
    @Fork(1)
    @BenchmarkMode(Mode.AverageTime)
    @OutputTimeUnit(TimeUnit.MICROSECONDS)
    @Warmup(iterations = 3, time = 3)
    @Measurement(iterations = 3, time = 3)
    public void fastutilSetRemove(MyState state) {
        IntStream.range(0, state.size).forEach(i -> {
            Order order = new Order(new Random().nextInt(state.size), i, i);
            state.fastutilSetBasedRepository.remove(order);
        });
    }

    @Benchmark
    @Fork(1)
    @BenchmarkMode(Mode.AverageTime)
    @OutputTimeUnit(TimeUnit.MICROSECONDS)
    @Warmup(iterations = 3, time = 3)
    @Measurement(iterations = 3, time = 3)
    public void kolobokeSetRemove(MyState state) {
        IntStream.range(0, state.size).forEach(i -> {
            Order order = new Order(new Random().nextInt(state.size), i, i);
            state.kolobokeSetBasedRepository.remove(order);
        });
    }

    @Benchmark
    @Fork(1)
    @BenchmarkMode(Mode.AverageTime)
    @OutputTimeUnit(TimeUnit.MICROSECONDS)
    @Warmup(iterations = 3, time = 3)
    @Measurement(iterations = 3, time = 3)
    public void troveSetRemove(MyState state) {
        IntStream.range(0, state.size).forEach(i -> {
            Order order = new Order(new Random().nextInt(state.size), i, i);
            state.troveSetBasedRepository.remove(order);
        });
    }

    @State(Scope.Thread)
    public static class MyState {
        @Param({"10", "100"})
        public int size;

        ArrayListBasedRepository<Order> arrayListBasedRepository;
        HashSetBasedRepository<Order> hashSetBasedRepository;
        TreeSetBasedRepository<Order> treeSetBasedRepository;
        EclipseListBasedRepository<Order> eclipseListBasedRepository;
        EclipseSetBasedRepository<Order> eclipseSetBasedRepository;
        FastutilSetBasedRepository<Order> fastutilSetBasedRepository;
        FastutilSetBasedRepository<Order> kolobokeSetBasedRepository;
        TroveSetBasedRepository<Order> troveSetBasedRepository;

        @Setup(Level.Iteration)
        public void doSetup() {
            arrayListBasedRepository = new ArrayListBasedRepository<>();
            hashSetBasedRepository = new HashSetBasedRepository<>();
            treeSetBasedRepository = new TreeSetBasedRepository<>();
            eclipseListBasedRepository = new EclipseListBasedRepository<>();
            eclipseSetBasedRepository = new EclipseSetBasedRepository<>();
            fastutilSetBasedRepository = new FastutilSetBasedRepository<>();
            kolobokeSetBasedRepository = new FastutilSetBasedRepository<>();
            troveSetBasedRepository = new TroveSetBasedRepository<>();

            IntStream.range(0, size / 2).forEach(i -> {
                Order order = new Order(new Random().nextInt(size), i, i);
                arrayListBasedRepository.add(order);
                hashSetBasedRepository.add(order);
                treeSetBasedRepository.add(order);
                eclipseListBasedRepository.add(order);
                eclipseSetBasedRepository.add(order);
                fastutilSetBasedRepository.add(order);
                kolobokeSetBasedRepository.add(order);
                troveSetBasedRepository.add(order);
            });
        }

//        @TearDown(Level.Iteration)
//        public void tearDown() {
//            System.out.println(arrayListBasedRepository.size() + " elements at tearDown.");
//        }

    }

}
