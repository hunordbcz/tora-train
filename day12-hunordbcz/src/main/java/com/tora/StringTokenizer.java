package com.tora;

import java.util.regex.Pattern;

public class StringTokenizer {

    private String string;
    private String delimiter;
    private String[] tokens;

    public StringTokenizer() {
        this(null, null);
    }

    public StringTokenizer(String string) {
        this(string, null);
    }

    public StringTokenizer(String string, String delimiter) {
        this.string = string;
        this.delimiter = delimiter;

        if (string != null && delimiter != null) {
            tokenize();
        } else {
            tokens = null;
        }
    }

    public int countTokens() {
        if (tokens == null) {
            return -1;
        }

        return tokens.length;
    }

    public String[] getTokens() throws IllegalStateException {
        validate();

        return tokens;
    }

    public String getTokenAt(int index) throws IllegalStateException, IndexOutOfBoundsException {
        validateIndex(index);

        return tokens[index];
    }

    public void setDelimiter(String delimiter) {
        this.delimiter = delimiter;
        if (string != null) {
            tokenize();
        }
    }

    public void setString(String string) {
        this.string = string;
        if (delimiter != null) {
            tokenize();
        }
    }

    private void validate() throws IllegalStateException {
        if (tokens == null) {
            if (string == null) {
                throw new IllegalStateException("String missing");
            }
            if (delimiter == null) {
                throw new IllegalStateException("Delimiter missing");
            }
        }
    }

    private void validateIndex(int index) throws IllegalStateException, IndexOutOfBoundsException {
        validate();

        if (index >= countTokens()) {
            throw new IndexOutOfBoundsException("Index bigger than token array size");
        }
    }

    private void tokenize() {
        //Remove delimiters from the start + end, so we won't have empty tokens
        string = string.replaceAll("^[" + delimiter + "]+|[" + delimiter + "]+$", "");

        tokens = string.split("[" + Pattern.quote(delimiter) + "]+");
    }
}
