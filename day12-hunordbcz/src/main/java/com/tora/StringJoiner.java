package com.tora;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class StringJoiner {

    private final List<String> tokens;
    private String delimiter;

    public StringJoiner() {
        this(null);
    }

    public StringJoiner(String delimiter) {
        this.delimiter = delimiter;
        this.tokens = new ArrayList<>();
    }

    public void addToken(String token) {
        tokens.add(token);
    }

    public void removeToken(String token) {
        tokens.removeAll(Collections.singletonList(token));
    }

    public void setDelimiter(String delimiter) {
        this.delimiter = delimiter;
    }

    public String getString() {
        if (delimiter == null) {
            throw new IllegalStateException("Delimiter is not set");
        }

        return String.join(delimiter, tokens);
    }
}
