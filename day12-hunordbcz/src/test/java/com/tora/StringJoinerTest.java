package com.tora;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class StringJoinerTest {

    @Rule
    public final ExpectedException exception = ExpectedException.none();
    private StringJoiner joiner;

    @Before
    public void setUp() {
        this.joiner = new StringJoiner();
    }

    @Test
    public void addToken() {
        joiner.setDelimiter(" ");
        joiner.addToken("Test");
        joiner.addToken("addToken");
        joiner.addToken("method");

        assertThat(joiner.getString(), is("Test addToken method"));
    }

    @Test
    public void removeToken() {
        joiner.setDelimiter("");
        joiner.removeToken("zero");
        assertThat(joiner.getString(), is(""));

        joiner.addToken("blank");
        joiner.removeToken("blank");
        assertThat(joiner.getString(), is(""));

        joiner.addToken("blank");
        joiner.addToken("blank");
        joiner.removeToken("blank");
        assertThat(joiner.getString(), is(""));
    }

    @Test
    public void setDelimiter() {
        joiner.setDelimiter(" ");
        assertThat(joiner.getString(), is(""));

        joiner.addToken("Test");
        joiner.addToken("setDelimiter");
        joiner.addToken("method");
        assertThat(joiner.getString(), is("Test setDelimiter method"));
    }

    @Test
    public void setDelimiterException() {
        exception.expect(IllegalStateException.class);
        exception.expectMessage("Delimiter is not set");
        joiner.getString();
    }

    @Test
    public void getString() {
        joiner.setDelimiter(" ");

        joiner.addToken("Test");
        joiner.addToken("getString");
        joiner.addToken("method");

        assertThat(joiner.getString(), is("Test getString method"));
    }

    @Test
    public void getStringException() {
        exception.expect(IllegalStateException.class);
        exception.expectMessage("Delimiter is not set");
        joiner.getString();
    }
}