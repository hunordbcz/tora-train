package com.tora;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class StringTokenizerTest {

    @Rule
    public final ExpectedException exception = ExpectedException.none();
    private StringTokenizer tokenizer;

    @Before
    public void setUp() {
        this.tokenizer = new StringTokenizer();
    }

    @Test
    public void testConstructor() {
        tokenizer = new StringTokenizer("Test constructor", " ");
        assertThat(tokenizer.getTokens(), is(new String[]{"Test", "constructor"}));

        tokenizer = new StringTokenizer("Test constructor");
        exception.expect(IllegalStateException.class);
        exception.expectMessage("Delimiter missing");
        tokenizer.getTokens();
    }

    @Test
    public void countTokens() {
        assertThat(tokenizer.countTokens(), is(-1));
        tokenizer.setDelimiter(" ");
        tokenizer.setString("Test countTokens method");
        assertThat(tokenizer.countTokens(), is(3));
    }

    @Test
    public void getTokens() {
        tokenizer.setString("Test getTokens method");
        tokenizer.setDelimiter(" ");
        assertThat(tokenizer.getTokens(), is(new String[]{"Test", "getTokens", "method"}));
    }

    @Test
    public void getTokensStringException() {
        exception.expect(IllegalStateException.class);
        exception.expectMessage("String missing");
        tokenizer.getTokens();
    }

    @Test
    public void getTokensDelimiterException() {
        exception.expect(IllegalStateException.class);
        exception.expectMessage("Delimiter missing");
        tokenizer.setString("Test getTokens method");
        tokenizer.getTokens();
    }

    @Test
    public void getTokenAt() {
        tokenizer.setString("Test getTokenAt method");
        tokenizer.setDelimiter(" ");

        assertThat(tokenizer.getTokenAt(0), is("Test"));
        assertThat(tokenizer.getTokenAt(1), is("getTokenAt"));
        assertThat(tokenizer.getTokenAt(2), is("method"));
    }

    @Test
    public void getTokenAtException() {
        tokenizer.setString("Test getTokenAt method");
        tokenizer.setDelimiter(" ");

        exception.expect(IndexOutOfBoundsException.class);
        exception.expectMessage("Index bigger than token array size");
        tokenizer.getTokenAt(3);
    }

    @Test
    public void setDelimiter() {
        tokenizer.setString("Test setDelimiter method");
        tokenizer.setDelimiter(" ");
        assertThat(tokenizer.getTokenAt(0), is("Test"));
    }

    @Test
    public void setDelimiterException() {
        tokenizer.setString("Test setDelimiter method");

        exception.expect(IllegalStateException.class);
        exception.expectMessage("Delimiter missing");
        tokenizer.getTokens();
    }

    @Test
    public void setString() {
        tokenizer.setDelimiter(" ");
        tokenizer.setString("Test setDelimiter method");
        assertThat(tokenizer.getTokenAt(0), is("Test"));
    }

    @Test
    public void setStringException() {
        tokenizer.setDelimiter(" ");

        exception.expect(IllegalStateException.class);
        exception.expectMessage("String missing");
        tokenizer.getTokens();
    }
}