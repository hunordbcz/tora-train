package com.tora;

import java.util.LinkedList;

public class ProducerConsumer {
    LinkedList<Integer> list = new LinkedList<>();

    public void produce() throws InterruptedException {
        int value = 0;
        while (true) {
            synchronized (this) {
                System.out.println("Producer "
                        + value);
                list.add(value++);
                notifyAll();

                Thread.sleep(1000);
            }
        }
    }

    public void consume() throws InterruptedException {
        while (true) {
            synchronized (this) {
                while (list.size() == 0) {
                    wait();
                }
                int val = list.removeFirst();

                System.out.println("Consumer consumed-"
                        + val);


                notify();
                Thread.sleep(1000);
            }
        }
    }
}
