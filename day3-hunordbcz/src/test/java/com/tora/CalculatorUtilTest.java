package com.tora;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class CalculatorUtilTest {

    @Rule
    public final ExpectedException exception = ExpectedException.none();

    @Test
    public void addSuccess() throws Exception {
        assertThat(CalculatorUtil.add(10,10), is(20));
    }

    @Test
    public void addNegativeNumbers() throws IllegalArgumentException {
        exception.expect(IllegalArgumentException.class);
        exception.expectMessage("Given numbers below the limit");
        CalculatorUtil.add(-1,10);
    }

    @Test
    public void addOverflow() throws IllegalArgumentException {
        exception.expect(IllegalArgumentException.class);
        exception.expectMessage("Given numbers above the limit");

        int nr = Integer.MAX_VALUE;
        CalculatorUtil.add(nr,nr);
    }
}