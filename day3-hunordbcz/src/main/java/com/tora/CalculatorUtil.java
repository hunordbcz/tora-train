package com.tora;

public class CalculatorUtil {

    private static final int UPPER_LIMIT = Integer.MAX_VALUE / 2;
    private static final int LOWER_LIMIT = 0;

    /**
     * Calculates the sum of two integers
     * @param x First Number
     * @param y Second Number
     * @return Sum of the two numbers
     * @throws IllegalArgumentException If any number is below/above the specified limits
     */
    public static int add(int x, int y) throws IllegalArgumentException {
        if (x < LOWER_LIMIT || y < LOWER_LIMIT) {
            throw new IllegalArgumentException("Given numbers below the limit");
        }

        if (x > UPPER_LIMIT || y > UPPER_LIMIT) {
            throw new IllegalArgumentException("Given numbers above the limit");
        }

        return x + y;
    }
}
