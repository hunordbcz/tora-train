package com.tora;

import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.IntStream;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class BigDecimalListTest {

    private BigDecimalList list;

    @Before
    public void setUp() throws Exception {
        LinkedList<BigDecimal> decimalList = new LinkedList<>();
        IntStream.range(0, 100).forEach(i -> {
            decimalList.add(BigDecimal.valueOf(i));
        });
        list = new BigDecimalList(decimalList);
    }

    @Test
    public void testCalculateSum() {
        assertThat(list.calculateSum(), is(BigDecimal.valueOf(4950)));
    }

    @Test
    public void testCalculateAverage() {
        assertThat(list.calculateAverage(), is(BigDecimal.valueOf(50)));
    }

    @Test
    public void testGetTopTenPercent() {
        List<BigDecimal> topTen = list.getTopTenPercent();
        List<BigDecimal> shouldBe = new LinkedList<>();
        IntStream.rangeClosed(1, 11).map(i -> 100 - i).forEach(i -> {
            shouldBe.add(BigDecimal.valueOf(i));
        });
        assertThat(topTen, is(shouldBe));
    }

    @Test
    public void testSize() {
        assertThat(list.size(), is(100L));
        list.add(BigDecimal.valueOf(100));
        assertThat(list.size(), is(101L));
    }

    @Test
    public void testNullElement() {
        assertThat(list.size(), is(100L));
        list.add(null);
        assertThat(list.size(), is(100L));
    }

    @Test
    public void add() {
        BigDecimalList list = new BigDecimalList();
        assertThat(list.size(), is(0L));
        BigDecimal obj = new BigDecimal(101);
        assertThat(list.add(obj), is(obj));
        assertThat(list.size(), is(1L));
    }
}