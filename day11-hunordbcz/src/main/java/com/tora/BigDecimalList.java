package com.tora;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class BigDecimalList {
    private final List<BigDecimal> list;

    public BigDecimalList(List<BigDecimal> list) {
        this.list = new LinkedList<>();
        list.stream().map(Objects::requireNonNull).forEach(this.list::add);
    }

    public BigDecimalList() {
        this.list = new LinkedList<>();
    }

    public BigDecimal add(BigDecimal obj) {
        if (obj == null) {
            return null;
        }
        list.add(obj);
        return obj;
    }

    public BigDecimal calculateSum() {
        return list.stream()
                .reduce(BigDecimal.ZERO, BigDecimal::add);
    }

    public BigDecimal calculateAverage() {
        return this.calculateSum()
                .divide(BigDecimal.valueOf(this.size()), RoundingMode.HALF_UP);
    }

    public List<BigDecimal> getTopTenPercent() {
        return list.stream().sorted(Comparator.reverseOrder()).limit(1 + this.size() / 10).collect(Collectors.toList());
    }

    public long size() {
        return list.size();
    }

}
