package com.tora;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.Map;
import java.util.Set;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class MultiMapTest {

    @Rule
    public final ExpectedException exception = ExpectedException.none();
    private MultiMap<String, String> multiMap;

    @Before
    public void setUp() throws Exception {
        multiMap = new MultiMap<>();
    }

    @After
    public void tearDown() throws Exception {
        multiMap.clear();
        multiMap = null;
    }

    @Test
    public void size() {
        assertThat(multiMap.size(), is(0));

        multiMap.put("key", "value");
        assertThat(multiMap.size(), is(1));

        multiMap.put("newKey", "value");
        assertThat(multiMap.size(), is(1));
    }

    @Test
    public void isEmpty() {
        assertThat(multiMap.isEmpty(), is(true));
    }

    @Test
    public void containsKey() {
        assertThat(multiMap.containsKey("false"), is(false));

        multiMap.put("key", "value");
        assertThat(multiMap.containsKey("key"), is(true));
    }

    @Test
    public void containsValue() {
        assertThat(multiMap.containsValue("key"), is(false));

        multiMap.put("key", "value");
        assertThat(multiMap.containsKey("key"), is(true));
    }

    @Test
    public void getSuccess() {
        multiMap.put("key", "value");
        assertThat(multiMap.get("key"), is("value"));

        multiMap.put("newKey", "value");
        assertThat(multiMap.get("newKey"), is("value"));
    }

    @Test
    public void getException() {
        exception.expect(NullPointerException.class);
        multiMap.get("key");
    }

    @Test
    public void putArray() {
        String[] keys = {"key1", "key2", "key3"};
        assertThat(multiMap.size(), is(0));
        assertThat(multiMap.put(keys, "value"), is("value"));
        assertThat(multiMap.size(), is(1));
        assertThat(multiMap.get("key1"), is("value"));
        assertThat(multiMap.get("key2"), is("value"));
        assertThat(multiMap.get("key3"), is("value"));
    }

    @Test
    public void put() {
        assertThat(multiMap.size(), is(0));
        assertThat(multiMap.put("key", "value"), is("value"));
        assertThat(multiMap.size(), is(1));
        assertThat(multiMap.get("key"), is("value"));
        assertThat(multiMap.put("key2", "value"), is("value"));
        assertThat(multiMap.size(), is(1));
        assertThat(multiMap.get("key2"), is("value"));
    }

    @Test
    public void remove() {
        multiMap.put("key", "value");
        assertThat(multiMap.keySet().size(), is(1));
        assertThat(multiMap.remove("key"), is("value"));
        assertThat(multiMap.keySet().size(), is(0));

        exception.expect(NullPointerException.class);
        assertThat(multiMap.remove("key"), is(null));
    }

    @Test
    public void putAll() {
        MultiMap<String, String> multiMap2 = new MultiMap<>();
        multiMap2.put("key", "value");
        multiMap2.put("key1", "value");
        multiMap2.put("key2", "value2");
        assertThat(multiMap2.size(), is(2));
        assertThat(multiMap.size(), is(0));
        multiMap.putAll(multiMap2);
        assertThat(multiMap.size(), is(2));
        assertThat(multiMap.get("key"), is("value"));
        assertThat(multiMap.get("key1"), is("value"));
        assertThat(multiMap.get("key2"), is("value2"));
    }

    @Test
    public void clear() {
        assertThat(multiMap.size(), is(0));

        multiMap.put("key1", "value");
        assertThat(multiMap.size(), is(1));

        multiMap.put("key2", "value");
        assertThat(multiMap.size(), is(1));

        multiMap.put("key3", "value2");
        assertThat(multiMap.size(), is(2));

        multiMap.clear();
        assertThat(multiMap.size(), is(0));
    }

    @Test
    public void keySet() {
        String[] keys = {"key1", "key2", "key3"};
        assertThat(multiMap.size(), is(0));
        assertThat(multiMap.put(keys, "value"), is("value"));
    }

    @Test
    public void values() {
        multiMap.put("test1", "value");
        multiMap.put("test2", "value");
        assertThat(multiMap.values().size(), is(1));
        assertThat(multiMap.containsValue("value"), is(true));
    }

    @Test
    public void entrySet() {
        multiMap.put("test1", "value");
        multiMap.put("test2", "value");
        Set<Map.Entry<String, String>> entrySet = multiMap.entrySet();
        assertThat(entrySet.size(), is(2));

        for (Map.Entry<String, String> entry : entrySet) {
            boolean contains = entry.getKey().equals("test1") || entry.getKey().equals("test2");
            assertThat(contains, is(true));
        }
    }
}