package com.tora;

import java.util.*;

public class MultiMap<K, V> implements Map<K, V> {
    private final Map<K, Integer> keyMap;
    private final Map<Integer, V> valueMap;

    public MultiMap() {
        this.keyMap = new HashMap<>();
        this.valueMap = new HashMap<>();
    }

    @Override
    public int size() {
        return valueMap.size();
    }

    @Override
    public boolean isEmpty() {
        return valueMap.isEmpty();
    }

    @Override
    public boolean containsKey(Object key) {
        return keyMap.containsKey(key);
    }

    @Override
    public boolean containsValue(Object value) {
        return valueMap.containsValue(value);
    }

    @Override
    public V get(Object key) {
        int hashCode = keyMap.get(key);
        return valueMap.get(hashCode);
    }

    public V put(K[] keys, V value) {
        int hashCode = value.hashCode();
        valueMap.put(hashCode, value);
        Arrays.stream(keys).forEach(k -> keyMap.put(k, hashCode));

        return value;
    }

    @Override
    public V put(K key, V value) {
        int hashCode = value.hashCode();
        valueMap.put(hashCode, value);
        keyMap.put(key, hashCode);
        return value;
    }

    @Override
    public V remove(Object key) {
        Optional<K> mainKey = keySet().stream().filter(k -> k.equals(key)).findFirst();
        if (!mainKey.isPresent()) {
            return null;
        }
        int hashCode = keyMap.get(mainKey.get());
        keyMap.entrySet().removeIf(k -> k.getValue().equals(hashCode));

        return valueMap.remove(hashCode);
    }

    @Override
    public void putAll(Map<? extends K, ? extends V> m) {
        m.forEach(this::put);
    }

    @Override
    public void clear() {
        valueMap.clear();
        keyMap.clear();
    }

    @Override
    public Set<K> keySet() {
        return keyMap.keySet();
    }

    @Override
    public Collection<V> values() {
        return valueMap.values();
    }

    @Override
    public Set<Entry<K, V>> entrySet() {
        Set<Entry<K, V>> set = new HashSet<>();
        keyMap.keySet().forEach(
                k -> set.add(Map.entry(k, get(k)))
        );
        return set;
    }
}
