package com.tora;

import com.tora.bll.Calculator;

public class App {
    public static void main(String[] args) {
        Calculator calculator;
        if (args.length > 0) {
            calculator = new Calculator(args[0]);
        } else {
            calculator = new Calculator();
        }
        calculator.run();
    }
}
