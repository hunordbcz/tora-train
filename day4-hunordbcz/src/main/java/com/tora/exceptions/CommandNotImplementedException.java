package com.tora.exceptions;

public class CommandNotImplementedException extends Exception {
    public CommandNotImplementedException(String message) {
        super(message);
    }
}
