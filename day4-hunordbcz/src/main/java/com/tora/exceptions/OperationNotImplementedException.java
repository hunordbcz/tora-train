package com.tora.exceptions;

public class OperationNotImplementedException extends Exception {
    public OperationNotImplementedException(String message) {
        super(message);
    }
}
