package com.tora;

import java.util.Scanner;

public class FileReader {
    private final Scanner in;

    public FileReader() {
        in = new Scanner(System.in);
    }

    public FileReader(String inputFile) {
        in = new Scanner(inputFile);
    }

    public String readLine() {
        System.out.print("\n> ");
        return in.nextLine();
    }
}
