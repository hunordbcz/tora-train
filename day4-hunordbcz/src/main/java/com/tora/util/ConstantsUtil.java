package com.tora.util;

public class ConstantsUtil {
    public static final String OPERATORS = "+*/-^%&|>";
    public static final String REGEX = "[+-]?(\\d+(\\.\\d+)?)|[+*/\\-^%&>|]";
    public static final String DOUBLE_REGEX = " ?(\\d+(\\.\\d+)?) ?";
    public static final String ONE_OPTION_OP = "(\\bsqrt\\b)";
    public static final String TWO_OPTION_OP = "(\\bmin|max|powerOf|round\\b)";
    public static final String ONE_OPTION_UNARY = "(" + ONE_OPTION_OP + " ?\\(" + DOUBLE_REGEX + "\\))";
    public static final String TWO_OPTION_UNARY = "(" + TWO_OPTION_OP + " ?\\(" + DOUBLE_REGEX + "," + DOUBLE_REGEX + "\\))";
    public static final String FULL_REGEX = REGEX + "|" + ONE_OPTION_UNARY + "|" + TWO_OPTION_UNARY;
    public static final double UPPER_LIMIT = Double.MAX_VALUE / 2;
    public static final double LOWER_LIMIT = -Double.MAX_VALUE / 2;
    private static int PRECISION = 4;

    private ConstantsUtil() {

    }

    public static int getPRECISION() {
        return PRECISION;
    }

    //todo use this
    public static void setPRECISION(int precision) {
        PRECISION = precision;
    }
}
