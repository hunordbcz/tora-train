package com.tora.util;

import com.tora.exceptions.InvalidInputException;

import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CalculatorUtil {
    private CalculatorUtil() {

    }

    public static List<String> exportNumbers(String source, String regex) throws InvalidInputException {
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(source);
        List<String> result = new LinkedList<>();
        while (matcher.find()) {
            result.add(matcher.group());
        }

        if (result.isEmpty()) {
            throw new InvalidInputException("Invalid input at " + source);
        }

        return result;
    }
}
