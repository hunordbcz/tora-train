package com.tora.util;

import com.tora.exceptions.InvalidInputException;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ValidatorUtil {
    private ValidatorUtil() {

    }

    public static boolean isOperator(char ch) {
        return ConstantsUtil.OPERATORS.indexOf(ch) != -1;
    }

    public static boolean isOperator(String string) {
        if (string.length() != 1) {
            return false;
        }

        return isOperator(string.charAt(0));
    }

    public static boolean isDouble(String string) {
        try {
            Double.parseDouble(string);
        } catch (NumberFormatException e) {
            return false;
        }

        return true;
    }

    public static boolean isExpression(String string) {
        return string.contains("sqrt") || string.contains("min") || string.contains("max") || string.contains("round") || string.contains("powerOf");
    }


    public static void validateLine(String toValidate) throws InvalidInputException {
        Pattern pattern = Pattern.compile(ConstantsUtil.FULL_REGEX);
        Matcher matcher = pattern.matcher(toValidate);

        String remaining = toValidate.replaceAll(ConstantsUtil.FULL_REGEX + "| ", "");
        if (remaining.length() > 0) {
            throw new InvalidInputException("Invalid input: Additional characters found: " + remaining);
        }

        boolean operatorShouldFollow = false;
        while (matcher.find()) {
            String element = matcher.group();
            if (operatorShouldFollow) {
                if (isOperator(element)) {
                    operatorShouldFollow = false;
                } else {
                    if (!isOperator(element.charAt(0))) {
                        throw new InvalidInputException("Invalid input: missing operator");
                    }
                }
            } else {
                if (!isDouble(element) && !isExpression(element)) {
                    throw new InvalidInputException("Invalid input: missing number");
                }
                operatorShouldFollow = true;
            }
        }
    }

    public static void validateNumber(double number) throws InvalidInputException {
        if (number >= ConstantsUtil.UPPER_LIMIT) {
            throw new InvalidInputException("Number " + number + " is above the upper limit");
        }
        if (number <= ConstantsUtil.LOWER_LIMIT) {
            throw new InvalidInputException("Number " + number + " is below the lower limit");
        }
    }
}
