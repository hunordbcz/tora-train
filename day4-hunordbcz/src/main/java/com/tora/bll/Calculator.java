package com.tora.bll;

import com.tora.FileReader;
import com.tora.dao.CalculatorDAO;
import com.tora.util.ValidatorUtil;

public class Calculator {
    private final FileReader fileReader;
    private final CalculatorDAO calculatorDAO;

    public Calculator(String input) {
        fileReader = new FileReader(input);
        calculatorDAO = new CalculatorDAO();
    }

    public Calculator() {
        fileReader = new FileReader();
        calculatorDAO = new CalculatorDAO();
    }

    public void run() {
        System.out.println("You may enter any numbers in any way on one line. \n" +
                "(Operation priority doesn't work)\n" +
                "To stop the program enter 'exit'");

        this.calculatorDAO.printHints();

        String line;
        while (!(line = fileReader.readLine()).equals("exit")) {
            try {
                ValidatorUtil.validateLine(line);
                double result = calculate(line);

                System.out.println("Result: " + result);
            } catch (Exception e) {
                System.err.println(e.getMessage());
            }
        }

    }

    public double calculate(String problem) throws Exception {
        return calculatorDAO.evaluate(problem);
    }


}
