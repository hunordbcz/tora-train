package com.tora.dao;

import com.tora.dao.math.MathCommand;
import com.tora.util.ConstantsUtil;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.tora.util.ValidatorUtil.isExpression;
import static com.tora.util.ValidatorUtil.isOperator;

public class CalculatorDAO {
    public double evaluate(String expression) throws Exception {
        Pattern pattern = Pattern.compile(ConstantsUtil.FULL_REGEX);
        Matcher matcher = pattern.matcher(expression);

        boolean operatorShouldFollow = false;
        double result = 0D;
        OPERATION operator = OPERATION.ADDITION;

        while (matcher.find()) {
            String element = matcher.group();
            if (operatorShouldFollow) {
                operator = OPERATION.getOperation(element.charAt(0));

                if (isOperator(element)) {
                    operatorShouldFollow = false;
                } else {
                    double number = Double.parseDouble(element.substring(1));
                    result = operator.execute(result, number);
                }

            } else {
                double number;
                if (isExpression(element)) {
                    String operation = element.substring(0, element.indexOf('('));
                    number = OPERATION.getOperation(operation).execute(element);
                } else {
                    number = Double.parseDouble(element);
                }

                result = operator.execute(result, number);
                operatorShouldFollow = true;
            }
        }
        return setPrecision(result);
    }

    private double setPrecision(double value) {
        return Double.parseDouble(String.format("%." + ConstantsUtil.getPRECISION() + "f", value));
    }

    public void printHints() {
        System.out.println("Supported operations:");
        for (OPERATION operation : OPERATION.values()) {
            MathCommand<Double> command = operation.getCommand();
            System.out.println(command.getOperator() + ": " + command.getFormat() + "\nExample: " + command.getExample() + "\n");
        }
    }
}
