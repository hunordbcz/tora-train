package com.tora.dao;

import com.tora.dao.math.*;
import com.tora.exceptions.InvalidOperationException;
import com.tora.exceptions.OperationNotImplementedException;

enum OPERATION {
    ADDITION(new AdditionCommand()),
    SUBTRACTION(new SubtractionCommand()),
    MULTIPLICATION(new MultiplicationCommand()),
    DIVISION(new DivisionCommand()),
    SQUARE_ROOT(new SquareRootCommand()),
    POWER(new PowerCommand()),
    MIN(new MinCommand()),
    ROUND(new RoundCommand()),
    MAX(new MaxCommand());

    private final MathCommand<Double> command;
    //todo set description here not in command
    OPERATION(MathCommand<Double> command) {
        this.command = command;
    }

    public static OPERATION getOperation(char op) throws OperationNotImplementedException {
        return getOperation(op + "");
    }

    public static OPERATION getOperation(String op) throws OperationNotImplementedException {
        switch (op) {
            case "+":
                return ADDITION;
            case "-":
                return SUBTRACTION;
            case "/":
                return DIVISION;
            case "*":
                return MULTIPLICATION;
            case "powerOf":
                return POWER;
            case "min":
                return MIN;
            case "max":
                return MAX;
            case "round":
                return ROUND;
            case "sqrt":
                return SQUARE_ROOT;

            default:
                throw new OperationNotImplementedException("Operation not implemented");
        }
    }

    public double execute(String expression) throws Exception {
        return this.command.executeString(expression);
    }

    public MathCommand<Double> getCommand() {
        return this.command;
    }

    public String getOperator() {
        return this.command.getOperator();
    }

    public double execute(double x, double y) throws Exception {
        if (this.command instanceof BinaryCommand) {
            return ((BinaryCommand<Double>) this.command).execute(x, y);
        }

        throw new InvalidOperationException("Operation " + this.command.getOperator() + " is not a Binary Command");
    }

    public double execute(double nr) throws Exception {
        if (this.command instanceof UnaryCommand) {
            return ((UnaryCommand<Double>) this.command).execute(nr);
        }

        throw new InvalidOperationException("Operation " + this.command.getOperator() + " is not a Unary Command");
    }
}