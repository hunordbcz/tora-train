package com.tora.dao.math;

public interface MathCommand<T extends Number> {
    T executeString(String expression) throws Exception;

    //fixme remove getOp/Form/Ex
    String getOperator();

    String getFormat();

    String getExample();

    default String getRegex() {
        return "[+-]?(\\d+(\\.\\d+)?)";
    }
}
