package com.tora.dao.math;

import com.tora.exceptions.InvalidInputException;
import com.tora.util.CalculatorUtil;
import com.tora.util.ValidatorUtil;

import java.util.List;

public class PowerCommand implements BinaryCommand<Double> {
    @Override
    public Double execute(Double number, Double power) throws InvalidInputException {
        validate(number, power);
        return Math.pow(number, power);
    }

    @Override
    public void validate(Double x, Double y) throws InvalidInputException {
        ValidatorUtil.validateNumber(x);
        ValidatorUtil.validateNumber(y);
    }

    @Override
    public Double executeString(String expression) throws InvalidInputException {
        List<String> elements = CalculatorUtil.exportNumbers(expression, this.getRegex());

        if (elements.size() < 2) {
            throw new InvalidInputException("Not enough elements for " + this.getOperator());
        }

        double x = Double.parseDouble(elements.get(0));
        double y = Double.parseDouble(elements.get(1));

        return this.execute(x, y);
    }

    @Override
    public String getOperator() {
        return "powerOf";
    }

    @Override
    public String getFormat() {
        return "powerOf(<number>,<power>)";
    }

    @Override
    public String getExample() {
        return "powerOf(2,10)";
    }
}
