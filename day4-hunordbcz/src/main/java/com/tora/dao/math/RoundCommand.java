package com.tora.dao.math;

import com.tora.exceptions.InvalidInputException;
import com.tora.util.CalculatorUtil;
import com.tora.util.ValidatorUtil;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

public class RoundCommand implements BinaryCommand<Double> {

    @Override
    public Double execute(Double nr, Double precision) throws InvalidInputException {
        validate(nr, precision);
        int precisionInt = precision.intValue();
        BigDecimal bd = new BigDecimal(Double.toString(nr));
        bd = bd.setScale(precisionInt, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

    @Override
    public void validate(Double nr, Double precision) throws InvalidInputException {
        if (precision < 0) throw new InvalidInputException("The rounding places must be greater or equal to 0");
        ValidatorUtil.validateNumber(nr);
        ValidatorUtil.validateNumber(precision);
    }

    @Override
    public Double executeString(String expression) throws InvalidInputException {
        List<String> elements = CalculatorUtil.exportNumbers(expression, this.getRegex());

        if (elements.size() < 2) {
            throw new InvalidInputException("Not enough elements for " + this.getOperator());
        }

        double x = Double.parseDouble(elements.get(0));
        double y = Integer.parseInt(elements.get(1));

        return this.execute(x, y);
    }

    @Override
    public String getOperator() {
        return "round";
    }

    @Override
    public String getFormat() {
        return "round(<number>,<precision>)";
    }

    @Override
    public String getExample() {
        return "round(12.15,1)";
    }
}
