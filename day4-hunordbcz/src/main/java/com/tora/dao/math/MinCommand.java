package com.tora.dao.math;

import com.tora.exceptions.InvalidInputException;
import com.tora.util.CalculatorUtil;
import com.tora.util.ValidatorUtil;

import java.util.List;

public class MinCommand implements BinaryCommand<Double> {
    @Override
    public Double execute(Double x, Double y) throws InvalidInputException {
        validate(x, y);
        return x < y ? x : y;
    }

    @Override
    public void validate(Double x, Double y) throws InvalidInputException {
        ValidatorUtil.validateNumber(x);
        ValidatorUtil.validateNumber(y);
    }

    @Override
    public Double executeString(String expression) throws InvalidInputException {
        List<String> elements = CalculatorUtil.exportNumbers(expression, this.getRegex());

        if (elements.size() < 2) {
            throw new InvalidInputException("Not enough elements for " + this.getOperator());
        }

        double x = Double.parseDouble(elements.get(0));
        double y = Double.parseDouble(elements.get(1));

        return this.execute(x, y);
    }

    @Override
    public String getOperator() {
        return "min";
    }

    @Override
    public String getFormat() {
        return "min(<number>,<number>)";
    }

    @Override
    public String getExample() {
        return "min(10,20)";
    }
}
