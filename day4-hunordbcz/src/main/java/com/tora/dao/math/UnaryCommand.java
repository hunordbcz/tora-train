package com.tora.dao.math;

import org.apache.commons.lang3.NotImplementedException;

public interface UnaryCommand<T extends Number> extends MathCommand<T> {
    default T execute(T nr) throws Exception {
        throw new NotImplementedException("Execute not implemented in class");
    }

    default void validate(T nr) throws Exception {
        throw new NotImplementedException("Validate not implemented in class");
    }
}
