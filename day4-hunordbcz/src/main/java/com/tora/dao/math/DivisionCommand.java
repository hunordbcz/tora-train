package com.tora.dao.math;

import com.tora.exceptions.DivisionByZeroException;
import com.tora.exceptions.InvalidOperationException;
import com.tora.util.CalculatorUtil;
import com.tora.util.ValidatorUtil;

import java.util.List;

public class DivisionCommand implements BinaryCommand<Double> {
    @Override
    public Double execute(Double x, Double y) throws Exception {
        validate(x, y);
        return x / y;
    }

    @Override
    public void validate(Double x, Double y) throws Exception {
        if (y == 0) {
            throw new DivisionByZeroException();
        }

        ValidatorUtil.validateNumber(x);
        ValidatorUtil.validateNumber(y);
    }

    @Override
    public Double executeString(String expression) throws Exception {
        List<String> elements = CalculatorUtil.exportNumbers(expression, this.getRegex() + "|[/]?");

        double x = Double.parseDouble(elements.get(0));
        double y;
        if (elements.size() == 3) {
            String operator = elements.get(1);
            if (!operator.equals(this.getOperator())) {
                throw new InvalidOperationException("Tried to do " + this.getOperator() + " while it should do " + operator);
            }
            y = Double.parseDouble(elements.get(2));
        } else {
            y = Double.parseDouble(elements.get(1));
        }

        return this.execute(x, y);
    }

    @Override
    public String getOperator() {
        return "/";
    }

    @Override
    public String getFormat() {
        return "<number> / <number>";
    }

    @Override
    public String getExample() {
        return null;
    }

    @Override
    public String getRegex() {
        return "[+-]?(\\d+(\\.\\d+)?)|[/]?";
    }
}
