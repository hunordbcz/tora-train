package com.tora.dao.math;

import com.tora.exceptions.InvalidInputException;
import com.tora.exceptions.InvalidOperationException;
import com.tora.util.CalculatorUtil;
import com.tora.util.ValidatorUtil;

import java.util.List;

public class MultiplicationCommand implements BinaryCommand<Double> {
    @Override
    public Double execute(Double x, Double y) throws InvalidInputException {
        validate(x, y);
        return x * y;
    }

    @Override
    public void validate(Double x, Double y) throws InvalidInputException {
        ValidatorUtil.validateNumber(x);
        ValidatorUtil.validateNumber(y);
    }

    @Override
    public Double executeString(String expression) throws InvalidInputException, InvalidOperationException {
        List<String> elements = CalculatorUtil.exportNumbers(expression, this.getRegex() + "|[*]?");

        double y;
        double x = Double.parseDouble(elements.get(0));

        if (elements.size() == 3) {
            String operator = elements.get(1);
            if (!operator.equals(this.getOperator())) {
                throw new InvalidOperationException("Tried to do " + this.getOperator() + " while it should do " + operator);
            }
            y = Double.parseDouble(elements.get(2));
        } else {
            y = Double.parseDouble(elements.get(1));
        }

        return this.execute(x, y);
    }

    @Override
    public String getOperator() {
        return "*";
    }

    @Override
    public String getFormat() {
        return "<number> * <number>";
    }

    @Override
    public String getExample() {
        return "3 * 6";
    }
}
