package com.tora.dao.math;

import org.apache.commons.lang3.NotImplementedException;

public interface BinaryCommand<T extends Number> extends MathCommand<T> {

    default T execute(T x, T y) throws Exception {
        throw new NotImplementedException("Execute not implemented in class");
    }

    default void validate(T x, T y) throws Exception {
        throw new NotImplementedException("Validate not implemented in class");
    }
}
