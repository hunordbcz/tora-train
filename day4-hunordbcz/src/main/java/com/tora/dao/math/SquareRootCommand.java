package com.tora.dao.math;

import com.tora.exceptions.InvalidInputException;
import com.tora.util.CalculatorUtil;
import com.tora.util.ValidatorUtil;

import java.util.List;

public class SquareRootCommand implements UnaryCommand<Double> {
    @Override
    public Double execute(Double nr) throws InvalidInputException {
        validate(nr);
        return Math.sqrt(nr);
    }

    @Override
    public void validate(Double nr) throws InvalidInputException {
        ValidatorUtil.validateNumber(nr);
    }

    @Override
    public Double executeString(String expression) throws InvalidInputException {
        List<String> elements = CalculatorUtil.exportNumbers(expression, this.getRegex());

        if (elements.size() < 1) {
            throw new InvalidInputException("Not enough elements for " + this.getOperator());
        }

        double number = Double.parseDouble(elements.get(0));
        return this.execute(number);
    }

    @Override
    public String getOperator() {
        return "sqrt";
    }

    @Override
    public String getFormat() {
        return "sqrt(<number>)";
    }

    @Override
    public String getExample() {
        return "sqrt(100)";
    }
}
