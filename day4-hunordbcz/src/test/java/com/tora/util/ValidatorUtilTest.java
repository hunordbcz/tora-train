package com.tora.util;

import com.tora.exceptions.InvalidInputException;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class ValidatorUtilTest {

    @Rule
    public final ExpectedException exception = ExpectedException.none();

    @Test
    public void isOperatorSuccess() {
        assertThat(ValidatorUtil.isOperator('+'), is(true));
        assertThat(ValidatorUtil.isOperator("+"), is(true));

        assertThat(ValidatorUtil.isOperator('-'), is(true));
        assertThat(ValidatorUtil.isOperator("-"), is(true));

        assertThat(ValidatorUtil.isOperator('/'), is(true));
        assertThat(ValidatorUtil.isOperator("/"), is(true));

        assertThat(ValidatorUtil.isOperator('*'), is(true));
        assertThat(ValidatorUtil.isOperator("*"), is(true));

        assertThat(ValidatorUtil.isOperator('^'), is(true));
        assertThat(ValidatorUtil.isOperator("^"), is(true));

        assertThat(ValidatorUtil.isOperator('%'), is(true));
        assertThat(ValidatorUtil.isOperator("%"), is(true));
    }

    @Test
    public void isOperatorFail() {
        assertThat(ValidatorUtil.isOperator('#'), is(false));
        assertThat(ValidatorUtil.isOperator("#"), is(false));

        assertThat(ValidatorUtil.isOperator('@'), is(false));
        assertThat(ValidatorUtil.isOperator("@"), is(false));
    }

    @Test
    public void isDoubleSuccess() {
        assertThat(ValidatorUtil.isDouble("123.12"), is(true));
    }

    @Test
    public void isDoubleFail() {
        assertThat(ValidatorUtil.isDouble("not-double"), is(false));
    }

    @Test(expected = Test.None.class)
    public void validateLineSuccess() throws InvalidInputException {
        ValidatorUtil.validateLine("12 - 3 / 6");
    }

    @Test
    public void validateLineFailMissingOperator() throws InvalidInputException {
        exception.expect(InvalidInputException.class);
        exception.expectMessage("Invalid input: missing operator");
        ValidatorUtil.validateLine("12 3 4 + 2");
    }

    @Test
    public void validateLineFailMissingNumber() throws InvalidInputException {
        exception.expect(InvalidInputException.class);
        exception.expectMessage("Invalid input: missing number");
        ValidatorUtil.validateLine("12 +++3");
    }

    @Test(expected = Test.None.class)
    public void validateNumberSuccess() throws InvalidInputException {
        ValidatorUtil.validateNumber(12.0D);
    }

    @Test
    public void validateNumberFailAboveLimit() throws InvalidInputException {
        double number = Double.MAX_VALUE;
        exception.expect(InvalidInputException.class);
        exception.expectMessage("Number " + number + " is above the upper limit");
        ValidatorUtil.validateNumber(number);
    }

    @Test
    public void validateNumberFailBelowLimit() throws InvalidInputException {
        double number = -Double.MAX_VALUE;
        exception.expect(InvalidInputException.class);
        exception.expectMessage("Number " + number + " is below the lower limit");
        ValidatorUtil.validateNumber(number);
    }
}