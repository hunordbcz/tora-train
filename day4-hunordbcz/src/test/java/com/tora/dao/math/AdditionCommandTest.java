package com.tora.dao.math;

import com.tora.exceptions.InvalidInputException;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class AdditionCommandTest {

    private AdditionCommand additionCommand;

    @Before
    public void setUp() {
        this.additionCommand = new AdditionCommand();
    }

    @Test
    public void execute() throws InvalidInputException {
        assertThat(additionCommand.execute(6D, 3D), is(9D));
    }

    @Test(expected = Test.None.class)
    public void validateSuccess() throws InvalidInputException {
        additionCommand.execute(1D, 2D);
    }
}