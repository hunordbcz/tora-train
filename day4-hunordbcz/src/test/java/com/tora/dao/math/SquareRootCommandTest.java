package com.tora.dao.math;

import com.tora.exceptions.InvalidInputException;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

public class SquareRootCommandTest {

    private SquareRootCommand squareRootCommand;

    @Before
    public void setUp() {
        this.squareRootCommand = new SquareRootCommand();
    }

    @Test
    public void execute() throws InvalidInputException {
        assertThat(squareRootCommand.execute(100D), is(10D));
    }

    @Test(expected = Test.None.class)
    public void validateSuccess() throws InvalidInputException {
        squareRootCommand.execute(1D);
    }
}