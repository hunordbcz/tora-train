package com.tora.dao.math;

import com.tora.exceptions.InvalidInputException;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

public class MinCommandTest {

    private MinCommand minCommand;

    @Before
    public void setUp() {
        this.minCommand = new MinCommand();
    }

    @Test
    public void execute() throws InvalidInputException {
        assertThat(minCommand.execute(-10D, 20D), is(-10D));
    }

    @Test(expected = Test.None.class)
    public void validateSuccess() throws InvalidInputException {
        minCommand.execute(1D, 2D);
    }
}