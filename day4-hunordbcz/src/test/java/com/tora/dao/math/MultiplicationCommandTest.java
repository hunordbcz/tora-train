package com.tora.dao.math;

import com.tora.exceptions.InvalidInputException;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class MultiplicationCommandTest {

    @Rule
    public final ExpectedException exception = ExpectedException.none();
    private MultiplicationCommand multiplicationCommand;

    @Before
    public void setUp() {
        this.multiplicationCommand = new MultiplicationCommand();
    }

    @Test
    public void execute() throws InvalidInputException {
        assertThat(multiplicationCommand.execute(6D, 3D), is(18D));
    }

    @Test(expected = Test.None.class)
    public void validateSuccess() throws InvalidInputException {
        multiplicationCommand.execute(1D, 2D);
    }
}