package com.tora.dao.math;

import com.tora.exceptions.InvalidInputException;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

public class RoundCommandTest {

    private RoundCommand roundCommand;

    @Before
    public void setUp() {
        this.roundCommand = new RoundCommand();
    }

    @Test
    public void execute() throws InvalidInputException {
        assertThat(roundCommand.execute(10.45D, 1D), is(10.5D));
    }

    @Test(expected = Test.None.class)
    public void validateSuccess() throws InvalidInputException {
        roundCommand.execute(1D, 2D);
    }
}