package com.tora.dao.math;

import com.tora.exceptions.InvalidInputException;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

public class PowerCommandTest {

    private PowerCommand powerCommand;

    @Before
    public void setUp() {
        this.powerCommand = new PowerCommand();
    }

    @Test
    public void execute() throws InvalidInputException {
        assertThat(powerCommand.execute(2D, 10D), is(1024D));
    }

    @Test(expected = Test.None.class)
    public void validateSuccess() throws InvalidInputException {
        powerCommand.execute(1D, 2D);
    }
}