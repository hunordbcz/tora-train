package com.tora.dao.math;

import com.tora.exceptions.InvalidInputException;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

public class MaxCommandTest {

    private MaxCommand maxCommand;

    @Before
    public void setUp() {
        this.maxCommand = new MaxCommand();
    }

    @Test
    public void execute() throws InvalidInputException {
        assertThat(maxCommand.execute(-10D, 20D), is(20D));
    }

    @Test(expected = Test.None.class)
    public void validateSuccess() throws InvalidInputException {
        maxCommand.execute(1D, 2D);
    }
}