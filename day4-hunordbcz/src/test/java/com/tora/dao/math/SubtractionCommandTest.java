package com.tora.dao.math;

import com.tora.exceptions.InvalidInputException;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class SubtractionCommandTest {

    private SubtractionCommand subtractionCommand;

    @Before
    public void setUp() {
        this.subtractionCommand = new SubtractionCommand();
    }

    @Test
    public void execute() throws InvalidInputException {
        assertThat(subtractionCommand.execute(6D, 3D), is(3D));
    }

    @Test(expected = Test.None.class)
    public void validateSuccess() throws InvalidInputException {
        subtractionCommand.execute(1D, 2D);
    }
}