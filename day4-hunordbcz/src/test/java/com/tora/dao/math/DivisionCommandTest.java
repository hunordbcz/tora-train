package com.tora.dao.math;

import com.tora.exceptions.DivisionByZeroException;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class DivisionCommandTest {

    @Rule
    public final ExpectedException exception = ExpectedException.none();
    private DivisionCommand divisionCommand;

    @Before
    public void setUp() {
        this.divisionCommand = new DivisionCommand();
    }

    @Test
    public void execute() throws Exception {
        assertThat(divisionCommand.execute(6D, 3D), is(2D));
    }

    @Test(expected = Test.None.class)
    public void validateSuccess() throws Exception {
        divisionCommand.execute(1D, 2D);
    }

    @Test
    public void validateThrowsDivisionByZero() throws Exception {
        exception.expect(DivisionByZeroException.class);
        exception.expectMessage("Division by zero occurred");
        divisionCommand.execute(1D, 0D);
    }
}