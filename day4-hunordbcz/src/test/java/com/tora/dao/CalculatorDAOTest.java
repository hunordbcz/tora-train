package com.tora.dao;

import com.tora.exceptions.DivisionByZeroException;
import com.tora.exceptions.OperationNotImplementedException;
import com.tora.util.ConstantsUtil;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class CalculatorDAOTest {

    @Rule
    public final ExpectedException exception = ExpectedException.none();
    private CalculatorDAO calculatorDAO;

    @Before
    public void setUp() {
        this.calculatorDAO = new CalculatorDAO();
    }

    @Test
    public void subtractExpression() throws Exception {
        assertThat(calculatorDAO.evaluate("12 - 3"), is(9.0D));
    }

    @Test
    public void addExpression() throws Exception {
        assertThat(calculatorDAO.evaluate("12 + 3"), is(15.0D));
        assertThat(calculatorDAO.evaluate("12 +3"), is(15.0D));
    }

    @Test
    public void multiplyExpression() throws Exception {
        assertThat(calculatorDAO.evaluate("12 / 3"), is(4.0D));
    }

    @Test
    public void divideExpression() throws Exception {
        assertThat(calculatorDAO.evaluate("12 * 3"), is(36.0D));
    }

    @Test
    public void operationNotImplementedExpression() throws Exception {
        exception.expect(OperationNotImplementedException.class);
        exception.expectMessage("Operation not implemented");
        calculatorDAO.evaluate("12 > 3");
    }

    @Test
    public void checkPrecision() throws Exception {
        String value = "0." + String.format("%" + ConstantsUtil.getPRECISION() + "c", ' ').replaceAll("\\ ", "\\" + '3');

        assertThat(calculatorDAO.evaluate("1 / 3"), is(Double.parseDouble(value)));
    }

    @Test
    public void evaluateThrowsDivisionByZero() throws Exception {
        exception.expect(DivisionByZeroException.class);
        exception.expectMessage("Division by zero occurred");
        calculatorDAO.evaluate("1/0");
    }
}